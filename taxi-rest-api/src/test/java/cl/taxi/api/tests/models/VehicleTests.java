package cl.taxi.api.tests.models;

import cl.taxi.api.dto.VehicleDTO;
import cl.taxi.api.models.Vehicle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ValidationException;

import java.util.Date;

@SpringBootTest
public class VehicleTests {

    private Vehicle vehicle;
    private VehicleDTO vehicleDTO;

    @BeforeEach
    public void setTestObject() {
        this.vehicle = new Vehicle();
        this.vehicleDTO = new VehicleDTO();
    }
    
    @Test
    public void shouldConstructObject() {
    	Date dateNow = new Date();
    	
    	vehicleDTO.setID(4);
    	vehicleDTO.setBrand("name");
    	vehicleDTO.setModel("lastname");
    	vehicleDTO.setColor("blue");
    	vehicleDTO.setSeats(4);
    	vehicleDTO.setRegistrationDate(dateNow);
    	vehicleDTO.setLicensePlate("1234-5678");
    	vehicleDTO.setActive(true);
    	
    	vehicle = new Vehicle(vehicleDTO);
    	Assertions.assertEquals(4, vehicle.getID());
    	Assertions.assertEquals("name", vehicle.getVehicleBrand());
    	Assertions.assertEquals("lastname", vehicle.getVehicleModel());
    	Assertions.assertEquals("blue", vehicle.getVehicleColor());
    	Assertions.assertEquals(4, vehicle.getVehicleSeats());
    	Assertions.assertEquals(dateNow, vehicle.getRegistrationDate());
    	Assertions.assertEquals("1234-5678", vehicle.getLicensePlate());
    	Assertions.assertTrue(vehicle.isActive());
    }
    
    @Test
    public void shouldSetID() {
    	vehicle.setID(4);
    	Assertions.assertEquals(4, vehicle.getID());
    }
    
    @Test
    public void shouldSetBrand() {
    	vehicle.setVehicleBrand("test");
    	Assertions.assertEquals("test", vehicle.getVehicleBrand());
    }
    
    @Test
    public void shouldSetModel() {
    	vehicle.setVehicleModel("test");
    	Assertions.assertEquals("test", vehicle.getVehicleModel());
    }
    
    @Test
    public void shouldSetColor() {
    	vehicle.setVehicleColor("blue");
    	Assertions.assertEquals("blue", vehicle.getVehicleColor());
    }
    
    @Test
    public void shouldSetSeats() {
    	vehicle.setVehicleSeats(4);
    	Assertions.assertEquals(4, vehicle.getVehicleSeats());
    }
    
    @Test
    public void shouldSetRegistrationDate() {
    	Date dateNow = new Date();
    	vehicle.setRegistrationDate(dateNow);
    	Assertions.assertEquals(dateNow, vehicle.getRegistrationDate());
    }
     
    @Test
    public void shouldSetLicensePlate() {
    	vehicle.setLicensePlate("test");
    	Assertions.assertEquals("test", vehicle.getLicensePlate());
    }
    
    @Test
    public void shouldSetActive() {
    	vehicle.setActive(false);
    	Assertions.assertFalse(vehicle.isActive());
    }
    
    @Test
    public void shouldValidateID() {
    	Assertions.assertThrows(ValidationException.class, () -> vehicle.validateID(null));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateID(0L));
    }

    @Test
    public void shouldValidateBrand() {
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateBrand(null));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateBrand("     "));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateBrand("a"));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateBrand(
        		"atertertetweewtewtewtwetewtwetwituwhetiwhtwituhwtuiwhtewwqwrqwrrerewruit" +
        		"atertertetweewtewtewtwetewtwetwituwhetiwhtwituhwtuiwhyweyewtwetqrqtewuit"
        ));
    }

    @Test
    public void shouldValidateModel() {
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateModel(null));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateModel("     "));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateModel("a"));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateModel(
        		"atertertetweewtewtewtwetewtwetwituwhetiwhtwituhwtuiwhtewwqwrqwrrerewruit" +
        		"atertertetweewtewtewtwetewtwetwituwhetiwhtwituhwtuiwhyweyewtwetqrqtewuit"
        ));
    }

    @Test
    public void shouldValidateColor() {
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateColor(null));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateColor("     "));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateColor("a"));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateColor(
        		"atertertetweewtewtewtwetewtwetwituwhetiwhtwituhwtuiwhtewwqwrqwrrerewruit" +
        		"atertertetweewtewtewtwetewtwetwituwhetiwhtwituhwtuiwhyweyewtwetqrqtewuit"
        ));
    }

    @Test
    public void shouldValidateSeats() {
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateSeats(null));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateSeats(1));
    }

    @Test
    public void shouldValidateLicensePlate() {
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateLicensePlate(null));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateLicensePlate("     "));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateLicensePlate("ab"));
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateLicensePlate(
        		"atertertetweewtewtewtwetewtwetwituwhetiwhtwituhwtuiwhtewwqwrqwrrerewruit" +
        		"atertertetweewtewtewtwetewtwetwituwhetiwhtwituhwtuiwhyweyewtwetqrqtewuit"
        ));
    }

    @Test
    public void shouldValidateActive() {
        Assertions.assertThrows(ValidationException.class, () -> vehicle.validateActive(null));
    }

}
