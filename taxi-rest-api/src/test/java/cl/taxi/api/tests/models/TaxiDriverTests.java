package cl.taxi.api.tests.models;

import cl.taxi.api.dto.TaxiDriverDTO;
import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.Vehicle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ValidationException;

import java.util.Date;

@SpringBootTest
public class TaxiDriverTests {

    private TaxiDriver taxiDriver;
    private TaxiDriverDTO taxiDriverDTO;
    
    private Vehicle vehicleFK;

    @BeforeEach
    public void setTestObject() {
        this.taxiDriver = new TaxiDriver();
        this.taxiDriverDTO = new TaxiDriverDTO();
        this.vehicleFK = new Vehicle();
    }
    
    @Test
    public void shouldConstructObject() {
    	Date joinDate = new Date();
    	vehicleFK.setID(5);
    	
    	taxiDriverDTO.setID(4);
    	taxiDriverDTO.setName("name");
    	taxiDriverDTO.setLastname("lastname");
    	taxiDriverDTO.setJoinDate(joinDate);
    	taxiDriverDTO.setRating(7.5);
    	taxiDriverDTO.setPhone("1234");
    	taxiDriverDTO.setVehicle(vehicleFK);
    	taxiDriverDTO.setJobsDone(8);
    	
    	taxiDriver = new TaxiDriver(taxiDriverDTO);
    	Assertions.assertEquals(4, taxiDriver.getID());
    	Assertions.assertEquals("name", taxiDriver.getFirstName());
    	Assertions.assertEquals("lastname", taxiDriver.getLastName());
    	Assertions.assertEquals(joinDate, taxiDriver.getJoinDate());
    	Assertions.assertEquals(7.5, taxiDriver.getDriverRating());
    	Assertions.assertEquals("1234", taxiDriver.getPhoneNumber());
    	Assertions.assertEquals(vehicleFK, taxiDriver.getVehicleFK());
    	Assertions.assertEquals(8, taxiDriver.getJobsPerformed());
    }
    
    @Test
    public void shouldSetID() {
    	taxiDriver.setID(5);
    	Assertions.assertEquals(5, taxiDriver.getID());
    }
    
    @Test
    public void shouldSetName() {
    	taxiDriver.setFirstName("test");
    	Assertions.assertEquals("test", taxiDriver.getFirstName());
    }
    
    @Test
    public void shouldSetLastName() {
    	taxiDriver.setLastName("test");
    	Assertions.assertEquals("test", taxiDriver.getLastName());
    }
    
    @Test
    public void shouldSetUserRating() {
    	taxiDriver.setDriverRating(7.5);
    	Assertions.assertEquals(7.5, taxiDriver.getDriverRating());
    }
    
    @Test
    public void shouldSetJoinDate() {
    	Date dateNow = new Date();
    	taxiDriver.setJoinDate(dateNow);
    	Assertions.assertEquals(dateNow, taxiDriver.getJoinDate());
    }
    
    @Test
    public void shouldSetPhoneNumber() {
    	taxiDriver.setPhoneNumber("+56 9 5205 6360");
    	Assertions.assertEquals("+56 9 5205 6360", taxiDriver.getPhoneNumber());
    }
    
    @Test
    public void shouldSetJobsPerformed() {
    	taxiDriver.setJobsPerformed(2);
    	Assertions.assertEquals(2, taxiDriver.getJobsPerformed());
    }
    
    @Test
    public void shouldSetVehicleForeignKey() {
    	vehicleFK.setID(6);
    	taxiDriver.setVehicleFK(vehicleFK);
    	Assertions.assertEquals(vehicleFK, taxiDriver.getVehicleFK());
    }
    
    @Test
    public void shouldValidateID() {
    	Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateID(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateID(0L));
    }

    @Test
    public void shouldValidateName() {
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateFirstName(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateFirstName("     "));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateFirstName("a"));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateFirstName("235235623623"));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateFirstName(
        		"weyweyweyweuhwetiuewhtiuewthweiuhweiurhweiuhyweiuhewtiuwehtiwuet" +
        		"ttwehitwuthwetiweuthweiuthweiuhwtwiuhtwiuheiwuthewiuthweiutwhtwiuhtwiuthwetiuwehtwiu"
        ));
    }

    @Test
    public void shouldValidateLastName() {
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateLastName(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateLastName("     "));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateLastName("a"));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateLastName("235235623623"));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateLastName(
        		"weyweyweyweuhwetiuewhtiuewthweiuhweiurhweiuhyweiuhewtiuwehtiwuet" +
        		"ttwehitwuthwetiweuthweiuthweiuhwtwiuhtwiuheiwuthewiuthweiutwhtwiuhtwiuthwetiuwehtwiu"
        ));
    }

    @Test
    public void shouldValidateTaxiDriverRating() {
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateRating(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateRating(-0.1));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateRating(10.1));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateRating(50.0));
    }

    @Test
    public void shouldValidatePhoneNumber() {
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validatePhoneNumber(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validatePhoneNumber("     "));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validatePhoneNumber("a"));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validatePhoneNumber("abc defg hijk "));
    }

    @Test
    public void shouldValidateJobsPerformed() {
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateJobs(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateJobs(-1));
    }

    @Test
    public void shouldValidateVehicleForeignKey() {
    	this.vehicleFK.setID(-1);
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateVehicle(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiDriver.validateVehicle(this.vehicleFK));
    }

}
