package cl.taxi.api.tests.integrationtests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@AutoConfigureMockMvc
@SpringBootTest
public class CustomerControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
    public void shouldGetAllCustomersResponse() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/customers")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertThat(statusCode, anyOf(equalTo(204), equalTo(200)));
    }
	
	@Test
    public void shouldGetCustomerResponse() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/customers/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertThat(statusCode, anyOf(equalTo(404), equalTo(200)));
    }
	
	@Test
    public void shouldGetCustomerResponseInvalidID() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/customers/0")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(400, statusCode);
    }
	
	@Test
    public void shouldGetCustomerResponseNotFound() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/customers/99999999")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(404, statusCode);
    }
		
	@Test
    public void shouldDeleteCustomerInvalidNotFound() throws Exception {
        MvcResult result = this.mockMvc.perform(delete("/customers/99999999")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(404, statusCode);
    }
		
}
