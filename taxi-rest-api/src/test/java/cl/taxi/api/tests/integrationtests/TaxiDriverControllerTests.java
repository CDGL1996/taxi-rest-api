package cl.taxi.api.tests.integrationtests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@AutoConfigureMockMvc
@SpringBootTest
public class TaxiDriverControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
    public void shouldGetAllTaxiDriversResponse() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/taxidrivers")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertThat(statusCode, anyOf(equalTo(204), equalTo(200)));
    }

    @Test
    public void shouldGetTaxiDriverResponse() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/taxidrivers/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertThat(statusCode, anyOf(equalTo(404), equalTo(200)));
    }
    
    @Test
    public void shouldGetTaxiDriverResponseInvalidID() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/taxidrivers/0")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(400, statusCode);
    }
    
    @Test
    public void shouldDeleteTaxiDriverInvalidNotFound() throws Exception {
        MvcResult result = this.mockMvc.perform(delete("/taxidrivers/99999999")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(404, statusCode);
    }

}
