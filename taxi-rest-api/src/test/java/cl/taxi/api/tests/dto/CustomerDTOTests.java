package cl.taxi.api.tests.dto;

import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import cl.taxi.api.dto.CustomerDTO;
import cl.taxi.api.models.Customer;

@SpringBootTest
public class CustomerDTOTests {
	
	private CustomerDTO customerDTO;
	private Customer customer;
	
	@BeforeEach
    public void setTestObject() {
        this.customerDTO = new CustomerDTO();
        this.customer = new Customer();
    }
	
	@Test
    public void shouldConstructObject() {
    	Date joinDate = new Date();
    	
    	customer.setID(4);
    	customer.setFirstName("name");
    	customer.setLastName("lastname");
    	customer.setUserRating(7.5);
    	customer.setJoinDate(joinDate);
    	customer.setPhoneNumber("1234");
    	customer.setEmail("abcd@gmail.com");
    	
    	customerDTO = new CustomerDTO(customer);
    	Assertions.assertEquals(4, customerDTO.getID());
    	Assertions.assertEquals("name", customerDTO.getName());
    	Assertions.assertEquals("lastname", customerDTO.getLastname());
    	Assertions.assertEquals(7.5, customerDTO.getRating());
    	Assertions.assertEquals(joinDate, customerDTO.getJoinDate());
    	Assertions.assertEquals("1234", customerDTO.getPhone());
    	Assertions.assertEquals("abcd@gmail.com", customerDTO.getEmail());
    }
	
	@Test
    public void shouldSetID() {
		customerDTO.setID(5);
    	Assertions.assertEquals(5, customerDTO.getID());
    }
	
	@Test
    public void shouldSetName() {
		customerDTO.setName("test");
    	Assertions.assertEquals("test", customerDTO.getName());
    }
    
    @Test
    public void shouldSetLastName() {
    	customerDTO.setLastname("test");
    	Assertions.assertEquals("test", customerDTO.getLastname());
    }
    
    @Test
    public void shouldSetUserRating() {
    	customerDTO.setRating(7.5);
    	Assertions.assertEquals(7.5, customerDTO.getRating());
    }
    
    @Test
    public void shouldSetJoinDate() {
    	Date date = new Date();
    	customerDTO.setJoinDate(date);
    	Assertions.assertEquals(date, customerDTO.getJoinDate());
    }
    
    @Test
    public void shouldSetPhoneNumber() {
    	customerDTO.setPhone("+56 9 5205 6360");
    	Assertions.assertEquals("+56 9 5205 6360", customerDTO.getPhone());
    }

}
