package cl.taxi.api.tests.dto;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import cl.taxi.api.dto.TaxiServiceDTO;
import cl.taxi.api.models.Customer;
import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.TaxiService;

@SpringBootTest
public class TaxiServiceDTOTests {
	
    private TaxiServiceDTO taxiServiceDTO;
	private TaxiService taxiService;
    private TaxiDriver taxiDriverFK;
    private Customer customerFK;
    
    @BeforeEach
    public void setTestObject() {
    	this.taxiServiceDTO = new TaxiServiceDTO();
        this.taxiService = new TaxiService();
        this.taxiDriverFK = new TaxiDriver();
        this.customerFK = new Customer();
    }
    
    @Test
    public void shouldConstructObject() {
    	Timestamp timeNow = new Timestamp(System.currentTimeMillis());
    	Timestamp timeLater = new Timestamp(System.currentTimeMillis());
    	timeLater.setTime(timeLater.getTime() + TimeUnit.MINUTES.toMillis(2));
    	
    	taxiDriverFK = new TaxiDriver(5);
    	customerFK = new Customer(3);
    	
    	taxiService.setID(5);
    	taxiService.setOrderTime(timeNow);
    	taxiService.setDriverFK(taxiDriverFK);
    	taxiService.setCustomerFK(customerFK);
    	taxiService.setPickupLocation("Location A");
    	taxiService.setDestination("Location B");
    	taxiService.setPickupTime(timeNow);
    	taxiService.setArrivalTime(timeLater);
    	taxiService.setDistanceKM(20.50);
    	taxiService.setPrice(2000.50);
    	taxiService.setServiceRating(8.5);
    	
    	taxiServiceDTO = new TaxiServiceDTO(taxiService);
    	Assertions.assertEquals(5, taxiServiceDTO.getID());
    	Assertions.assertEquals(timeNow, taxiServiceDTO.getOrderTime());
    	Assertions.assertEquals(taxiDriverFK, taxiServiceDTO.getDriver());
    	Assertions.assertEquals(customerFK, taxiServiceDTO.getCustomer());
    	Assertions.assertEquals("Location A", taxiServiceDTO.getPickupLocation());
    	Assertions.assertEquals("Location B", taxiServiceDTO.getDestination());
    	Assertions.assertEquals(timeNow, taxiServiceDTO.getPickup());
    	Assertions.assertEquals(timeLater, taxiServiceDTO.getArrival());
    	Assertions.assertEquals(20.50, taxiServiceDTO.getKilometers());
    	Assertions.assertEquals(2000.50, taxiServiceDTO.getPrice());
    	Assertions.assertEquals(8.5, taxiServiceDTO.getServiceRating());
    }
    
    @Test
    public void shouldSetID() {
    	taxiServiceDTO.setID(4);
    	Assertions.assertEquals(4, taxiServiceDTO.getID());
    }
    
    @Test
    public void shouldSetOrderTime() {
    	Timestamp timeNow = new Timestamp(System.currentTimeMillis());
    	taxiServiceDTO.setOrderTime(timeNow);
    	Assertions.assertEquals(timeNow, taxiServiceDTO.getOrderTime());
    }
    
    @Test
    public void shouldSetDriverForeignKey() {
    	taxiDriverFK.setID(5);
    	taxiServiceDTO.setDriver(taxiDriverFK);
    	Assertions.assertEquals(taxiDriverFK, taxiServiceDTO.getDriver());
    }
    
    @Test
    public void shouldSetCustomerForeignKey() {
    	customerFK.setID(3);
    	taxiServiceDTO.setCustomer(customerFK);
    	Assertions.assertEquals(customerFK, taxiServiceDTO.getCustomer());
    }
    
    @Test
    public void shouldSetPickupLocation() {
    	taxiServiceDTO.setPickupLocation("A");
    	Assertions.assertEquals("A", taxiServiceDTO.getPickupLocation());
    }
    
    @Test
    public void shouldSetDestination() {
    	taxiServiceDTO.setDestination("B");
    	Assertions.assertEquals("B", taxiServiceDTO.getDestination());
    }
    
    @Test
    public void shouldSetPickupTime() {
    	Timestamp timeNow = new Timestamp(System.currentTimeMillis());
    	taxiServiceDTO.setPickup(timeNow);
    	Assertions.assertEquals(timeNow, taxiServiceDTO.getPickup());
    }
    
    @Test
    public void shouldSetArrivalTime() {
    	Timestamp timeNow = new Timestamp(System.currentTimeMillis());
    	taxiServiceDTO.setArrival(timeNow);
    	Assertions.assertEquals(timeNow, taxiServiceDTO.getArrival());
    }
    
    @Test
    public void shouldSetDistance() {
    	taxiServiceDTO.setKilometers(8.5);
    	Assertions.assertEquals(8.5, taxiServiceDTO.getKilometers());
    }
    
    @Test
    public void shouldSetPrice() {
    	taxiServiceDTO.setPrice(50.50);
    	Assertions.assertEquals(50.50, taxiServiceDTO.getPrice());
    }
    
    @Test
    public void shouldSetRating() {
    	taxiServiceDTO.setServiceRating(6.2);
    	Assertions.assertEquals(6.2, taxiServiceDTO.getServiceRating());
    }

}
