package cl.taxi.api.tests.integrationtests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@AutoConfigureMockMvc
@SpringBootTest
public class TaxiServiceControllerTests {
	
	@Autowired
    private MockMvc mockMvc;
	
	@Test
    public void shouldGetAllTaxiServicesResponse() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/taxiservices/joe/test")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertThat(statusCode, anyOf(equalTo(404), equalTo(200)));
    }
	
	@Test
    public void shouldGetAllTaxiServicesResponseNamesTooShort() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/taxiservices/a/b")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(400, statusCode);
    }
	
    @Test
    public void shouldGetTaxiServiceByDateResponse() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/taxiservices/date/25-07-2030")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertThat(statusCode, anyOf(equalTo(404), equalTo(200)));
    }

    @Test
    public void shouldGetTaxiServiceByIDResponse() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/taxiservices/9999")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertThat(statusCode, anyOf(equalTo(404), equalTo(200)));
    }
    
    @Test
    public void shouldGetTaxiServiceByIDResponseInvalidID() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/taxiservices/0")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(400, statusCode);
    }
    
    @Test
    public void shouldDeleteTaxiServiceInvalidNotFound() throws Exception {
        MvcResult result = this.mockMvc.perform(delete("/taxiservices/99999999")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(404, statusCode);
    }

}
