package cl.taxi.api.tests.dto;

import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import cl.taxi.api.dto.TaxiDriverDTO;
import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.Vehicle;

@SpringBootTest
public class TaxiDriverDTOTests {
	
	private TaxiDriverDTO taxiDriverDTO;
	private TaxiDriver taxiDriver;
	private Vehicle vehicleFK;
	
	@BeforeEach
    public void setTestObject() {
        this.taxiDriverDTO = new TaxiDriverDTO();
        this.taxiDriver = new TaxiDriver();
        this.vehicleFK = new Vehicle();
    }
	
	@Test
	public void shouldConstructObject() {
		Date joinDate = new Date();
	    vehicleFK.setID(5);
	    	
	    taxiDriver.setID(4);
	    taxiDriver.setFirstName("name");
	    taxiDriver.setLastName("lastname");
	    taxiDriver.setJoinDate(joinDate);
	    taxiDriver.setDriverRating(7.5);
	    taxiDriver.setPhoneNumber("1234");
	    taxiDriver.setVehicleFK(vehicleFK);
	    taxiDriver.setJobsPerformed(8);
	    	
	    taxiDriverDTO = new TaxiDriverDTO(taxiDriver);
	    Assertions.assertEquals(4, taxiDriverDTO.getID());
	    Assertions.assertEquals("name", taxiDriverDTO.getName());
	    Assertions.assertEquals("lastname", taxiDriverDTO.getLastname());
	    Assertions.assertEquals(joinDate, taxiDriverDTO.getJoinDate());
	    Assertions.assertEquals(7.5, taxiDriverDTO.getRating());
	    Assertions.assertEquals("1234", taxiDriverDTO.getPhone());
	    Assertions.assertEquals(vehicleFK, taxiDriverDTO.getVehicle());
	    Assertions.assertEquals(8, taxiDriverDTO.getJobsDone());
	}
	
	@Test
    public void shouldSetID() {
		taxiDriverDTO.setID(5);
    	Assertions.assertEquals(5, taxiDriverDTO.getID());
    }
    
    @Test
    public void shouldSetName() {
    	taxiDriverDTO.setName("test");
    	Assertions.assertEquals("test", taxiDriverDTO.getName());
    }
    
    @Test
    public void shouldSetLastName() {
    	taxiDriverDTO.setLastname("test");
    	Assertions.assertEquals("test", taxiDriverDTO.getLastname());
    }
    
    @Test
    public void shouldSetUserRating() {
    	taxiDriverDTO.setRating(7.5);
    	Assertions.assertEquals(7.5, taxiDriverDTO.getRating());
    }
    
    @Test
    public void shouldSetJoinDate() {
    	taxiDriverDTO.setJoinDate(new Date());
    	Assertions.assertEquals(new Date(), taxiDriverDTO.getJoinDate());
    }
    
    @Test
    public void shouldSetPhoneNumber() {
    	taxiDriverDTO.setPhone("+56 9 5205 6360");
    	Assertions.assertEquals("+56 9 5205 6360", taxiDriverDTO.getPhone());
    }
    
    @Test
    public void shouldSetJobsPerformed() {
    	taxiDriverDTO.setJobsDone(2);
    	Assertions.assertEquals(2, taxiDriverDTO.getJobsDone());
    }
    
    @Test
    public void shouldSetVehicleForeignKey() {
    	vehicleFK.setID(6);
    	taxiDriverDTO.setVehicle(vehicleFK);
    	Assertions.assertEquals(vehicleFK, taxiDriverDTO.getVehicle());
    }
	
}
