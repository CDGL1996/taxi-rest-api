package cl.taxi.api.tests.models;

import cl.taxi.api.dto.CustomerDTO;
import cl.taxi.api.models.Customer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ValidationException;

import java.util.Date;

@SpringBootTest
public class CustomerTests {

    private Customer customer;
    private CustomerDTO customerDTO;

    @BeforeEach
    public void setTestObject() {
        this.customer = new Customer();
        this.customerDTO = new CustomerDTO();
    }
    
    @Test
    public void shouldConstructObject() {
    	Date joinDate = new Date();
    	
    	customerDTO.setID(4);
    	customerDTO.setName("name");
    	customerDTO.setLastname("lastname");
    	customerDTO.setRating(7.5);
    	customerDTO.setJoinDate(joinDate);
    	customerDTO.setPhone("1234");
    	customerDTO.setEmail("abcd@gmail.com");
    	
    	customer = new Customer(customerDTO);
    	Assertions.assertEquals(4, customer.getID());
    	Assertions.assertEquals("name", customer.getFirstName());
    	Assertions.assertEquals("lastname", customer.getLastName());
    	Assertions.assertEquals(7.5, customer.getUserRating());
    	Assertions.assertEquals(joinDate, customer.getJoinDate());
    	Assertions.assertEquals("1234", customer.getPhoneNumber());
    	Assertions.assertEquals("abcd@gmail.com", customer.getEmail());
    }
    
    @Test
    public void shouldSetID() {
    	customer.setID(5);
    	Assertions.assertEquals(5, customer.getID());
    }
    
    @Test
    public void shouldSetName() {
    	customer.setFirstName("test");
    	Assertions.assertEquals("test", customer.getFirstName());
    }
    
    @Test
    public void shouldSetLastName() {
    	customer.setLastName("test");
    	Assertions.assertEquals("test", customer.getLastName());
    }
    
    @Test
    public void shouldSetUserRating() {
    	customer.setUserRating(7.5);
    	Assertions.assertEquals(7.5, customer.getUserRating());
    }
    
    @Test
    public void shouldSetJoinDate() {
    	Date date = new Date();
    	customer.setJoinDate(date);
    	Assertions.assertEquals(date, customer.getJoinDate());
    }
    
    @Test
    public void shouldSetPhoneNumber() {
    	customer.setPhoneNumber("+56 9 5205 6360");
    	Assertions.assertEquals("+56 9 5205 6360", customer.getPhoneNumber());
    }
    
    @Test
    public void emailSetterTest() {
    	customer.setEmail("abcd@gmail.com");
    	Assertions.assertEquals("abcd@gmail.com", customer.getEmail());
    }
    
    @Test
    public void shouldValidateID() {
    	Assertions.assertThrows(ValidationException.class, () -> customer.validateID(null));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateID(0L));
    }

    @Test
    public void shouldValidateName() {
        Assertions.assertThrows(ValidationException.class, () -> customer.validateFirstName(null));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateFirstName("     "));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateFirstName("a"));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateFirstName("235235623623"));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateFirstName(
        		"weyweyweyweuhwetiuewhtiuewthweiuhweiurhweiuhyweiuhewtiuwehtiwuet" +
        		"ttwehitwuthwetiweuthweiuthweiuhwtwiuhtwiuheiwuthewiuthweiutwhtwiuhtwiuthwetiuwehtwiu"
        ));
    }

    @Test
    public void shouldValidateLastName() {
        Assertions.assertThrows(ValidationException.class, () -> customer.validateLastName(null));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateLastName("     "));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateLastName("a"));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateLastName("235235623623"));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateLastName(
        		"weyweyweyweuhwetiuewhtiuewthweiuhweiurhweiuhyweiuhewtiuwehtiwuet" +
        		"ttwehitwuthwetiweuthweiuthweiuhwtwiuhtwiuheiwuthewiuthweiutwhtwiuhtwiuthwetiuwehtwiu"
        ));
    }

    @Test
    public void shouldValidateCustomerRating() {
        Assertions.assertThrows(ValidationException.class, () -> customer.validateRating(null));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateRating(-0.1));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateRating(10.1));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateRating(50.0));
    }

    @Test
    public void shouldValidatePhoneNumber() {
        Assertions.assertThrows(ValidationException.class, () -> customer.validatePhoneNumber(null));
        Assertions.assertThrows(ValidationException.class, () -> customer.validatePhoneNumber("     "));
        Assertions.assertThrows(ValidationException.class, () -> customer.validatePhoneNumber("a"));
        Assertions.assertThrows(ValidationException.class, () -> customer.validatePhoneNumber("abc defg hijk "));
    }

    @Test
    public void shouldValidateEmail() {
        Assertions.assertThrows(ValidationException.class, () -> customer.validateEmail(null));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateEmail("     "));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateEmail("a"));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateEmail("cdgl1996gmail.com"));
        Assertions.assertThrows(ValidationException.class, () -> customer.validateEmail("wetewtwotejwiuhweiuhewituewhweiuthwyiheryiurehtiuthwiur"));
    }

}
