package cl.taxi.api.tests.services;

import javax.validation.ValidationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import cl.taxi.api.dto.VehicleDTO;
import cl.taxi.api.services.VehicleService;

@SpringBootTest
public class VehicleServiceTests {
	
	@Autowired
	private VehicleService vehicleService;
	
	private VehicleDTO vehicleDTO;
	
	@BeforeEach
	public void setupObject() {
		this.vehicleDTO = new VehicleDTO();
	}
	
	@Test
	public void shouldValidateGetVehicles() {
		Assertions.assertNotEquals(Exception.class, vehicleService.getVehicles());
	}
	
	@Test
	public void shouldValidateGetVehicleByLicensePlate() {
		Assertions.assertThrows(ValidationException.class, () -> vehicleService.getVehicle(null));
		Assertions.assertThrows(ValidationException.class, () -> vehicleService.getVehicle("ab"));
		Assertions.assertThrows(ValidationException.class, () -> vehicleService.getVehicle("                "));
		Assertions.assertNotEquals(Exception.class, vehicleService.getVehicle("ABC-DEF-123"));
	}
	
	@Test
	public void shouldValidateAddVehicle() {
		vehicleDTO.setBrand("Chevrolet");
		vehicleDTO.setModel("Cheyenne");
		vehicleDTO.setColor("Green");
		vehicleDTO.setSeats(4);
		vehicleDTO.setLicensePlate("ABC-DEF");
		vehicleDTO.setActive(null);
		Assertions.assertThrows(ValidationException.class, () -> vehicleService.addVehicle(vehicleDTO));		
	}
	
	@Test
	public void shouldValidateUpdateVehicle() {
		vehicleDTO.setID(999999L);
		vehicleDTO.setBrand("Chevrolet");
		vehicleDTO.setModel("Cheyenne");
		vehicleDTO.setColor("Green");
		vehicleDTO.setSeats(4);
		vehicleDTO.setLicensePlate("ABC-DEF");
		vehicleDTO.setActive(false);
		Assertions.assertThrows(ValidationException.class, () -> vehicleService.updateVehicle(0, vehicleDTO));
		Assertions.assertNotEquals(ValidationException.class, vehicleService.updateVehicle(vehicleDTO.getID(), vehicleDTO));		
	}
	
	@Test
	public void shouldValidateDeleteVehicle() {
		Assertions.assertThrows(ValidationException.class, () -> vehicleService.deleteVehicle(0));
		Assertions.assertNotEquals(ValidationException.class, vehicleService.deleteVehicle(999999L));
	}

}
