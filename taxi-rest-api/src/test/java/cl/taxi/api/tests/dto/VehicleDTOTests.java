package cl.taxi.api.tests.dto;

import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import cl.taxi.api.dto.VehicleDTO;
import cl.taxi.api.models.Vehicle;

@SpringBootTest
public class VehicleDTOTests {
	
	private VehicleDTO vehicleDTO;
	private Vehicle vehicle;
	
	@BeforeEach
    public void setTestObject() {
        this.vehicleDTO = new VehicleDTO();
        this.vehicle = new Vehicle();
    }
	
	@Test
    public void shouldConstructObject() {
    	Date dateNow = new Date();
    	
    	vehicle.setID(4);
    	vehicle.setVehicleBrand("name");
    	vehicle.setVehicleModel("lastname");
    	vehicle.setVehicleColor("blue");
    	vehicle.setVehicleSeats(4);
    	vehicle.setRegistrationDate(dateNow);
    	vehicle.setLicensePlate("1234-5678");
    	vehicle.setActive(true);
    	
    	vehicleDTO = new VehicleDTO(vehicle);
    	Assertions.assertEquals(4, vehicleDTO.getID());
    	Assertions.assertEquals("name", vehicleDTO.getBrand());
    	Assertions.assertEquals("lastname", vehicleDTO.getModel());
    	Assertions.assertEquals("blue", vehicleDTO.getColor());
    	Assertions.assertEquals(4, vehicleDTO.getSeats());
    	Assertions.assertEquals(dateNow, vehicleDTO.getRegistrationDate());
    	Assertions.assertEquals("1234-5678", vehicleDTO.getLicensePlate());
    	Assertions.assertTrue(vehicleDTO.isActive());
    }
	
	@Test
    public void shouldSetID() {
		vehicleDTO.setID(4);
    	Assertions.assertEquals(4, vehicleDTO.getID());
    }
    
    @Test
    public void shouldSetBrand() {
    	vehicleDTO.setBrand("test");
    	Assertions.assertEquals("test", vehicleDTO.getBrand());
    }
    
    @Test
    public void shouldSetModel() {
    	vehicleDTO.setModel("test");
    	Assertions.assertEquals("test", vehicleDTO.getModel());
    }
    
    @Test
    public void shouldSetColor() {
    	vehicleDTO.setColor("blue");
    	Assertions.assertEquals("blue", vehicleDTO.getColor());
    }
    
    @Test
    public void shouldSetSeats() {
    	vehicleDTO.setSeats(4);
    	Assertions.assertEquals(4, vehicleDTO.getSeats());
    }
    
    @Test
    public void shouldSetRegistrationDate() {
    	Date dateNow = new Date();
    	vehicleDTO.setRegistrationDate(dateNow);
    	Assertions.assertEquals(dateNow, vehicleDTO.getRegistrationDate());
    }
        
    @Test
    public void shouldSetLicensePlate() {
    	vehicleDTO.setLicensePlate("test");
    	Assertions.assertEquals("test", vehicleDTO.getLicensePlate());
    }
    
    @Test
    public void shouldSetActive() {
    	vehicleDTO.setActive(false);
    	Assertions.assertFalse(vehicleDTO.isActive());
    }

}
