package cl.taxi.api.tests.models;

import cl.taxi.api.dto.TaxiServiceDTO;
import cl.taxi.api.models.Customer;
import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.TaxiService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ValidationException;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@AutoConfigureMockMvc
@SpringBootTest
public class TaxiServiceTests {

    private TaxiService taxiService;
    private TaxiServiceDTO taxiServiceDTO;
    private TaxiDriver taxiDriverFK;
    private Customer customerFK;

    @BeforeEach
    public void setTestObject() {
        this.taxiService = new TaxiService();
        this.taxiServiceDTO = new TaxiServiceDTO();
        this.taxiDriverFK = new TaxiDriver();
        this.customerFK = new Customer();
    }
    
    @Test
    public void shouldConstructObject() {
    	Timestamp timeNow = new Timestamp(System.currentTimeMillis());
    	Timestamp timeLater = new Timestamp(System.currentTimeMillis());
    	timeLater.setTime(timeLater.getTime() + TimeUnit.MINUTES.toMillis(2));
    	
    	taxiDriverFK = new TaxiDriver(5);
    	customerFK = new Customer(3);
    	
    	taxiServiceDTO.setID(5);
    	taxiServiceDTO.setOrderTime(timeNow);
    	taxiServiceDTO.setDriver(taxiDriverFK);
    	taxiServiceDTO.setCustomer(customerFK);
    	taxiServiceDTO.setPickupLocation("Location A");
    	taxiServiceDTO.setDestination("Location B");
    	taxiServiceDTO.setPickup(timeNow);
    	taxiServiceDTO.setArrival(timeLater);
    	taxiServiceDTO.setKilometers(20.50);
    	taxiServiceDTO.setPrice(2000.50);
    	taxiServiceDTO.setServiceRating(8.5);
    	
    	taxiService = new TaxiService(taxiServiceDTO);
    	Assertions.assertEquals(5, taxiService.getID());
    	Assertions.assertEquals(timeNow, taxiService.getOrderTime());
    	Assertions.assertEquals(taxiDriverFK, taxiService.getDriverFK());
    	Assertions.assertEquals(customerFK, taxiService.getCustomerFK());
    	Assertions.assertEquals("Location A", taxiService.getPickupLocation());
    	Assertions.assertEquals("Location B", taxiService.getDestination());
    	Assertions.assertEquals(timeNow, taxiService.getPickupTime());
    	Assertions.assertEquals(timeLater, taxiService.getArrivalTime());
    	Assertions.assertEquals(20.50, taxiService.getDistanceKM());
    	Assertions.assertEquals(2000.50, taxiService.getPrice());
    	Assertions.assertEquals(8.5, taxiService.getServiceRating());
    }
    
    @Test
    public void shouldSetID() {
    	taxiService.setID(4);
    	Assertions.assertEquals(4, taxiService.getID());
    }
    
    @Test
    public void shouldSetOrderTime() {
    	Timestamp timeNow = new Timestamp(System.currentTimeMillis());
    	taxiService.setOrderTime(timeNow);
    	Assertions.assertEquals(timeNow, taxiService.getOrderTime());
    }
    
    @Test
    public void shouldSetDriverForeignKey() {
    	taxiDriverFK.setID(5);
    	taxiService.setDriverFK(taxiDriverFK);
    	Assertions.assertEquals(taxiDriverFK, taxiService.getDriverFK());
    }
    
    @Test
    public void shouldSetCustomerForeignKey() {
    	customerFK.setID(3);
    	taxiService.setCustomerFK(customerFK);
    	Assertions.assertEquals(customerFK, taxiService.getCustomerFK());
    }
    
    @Test
    public void shouldSetPickupLocation() {
    	taxiService.setPickupLocation("A");
    	Assertions.assertEquals("A", taxiService.getPickupLocation());
    }
    
    @Test
    public void shouldSetDestination() {
    	taxiService.setDestination("B");
    	Assertions.assertEquals("B", taxiService.getDestination());
    }
    
    @Test
    public void shouldSetPickupTime() {
    	Timestamp timeNow = new Timestamp(System.currentTimeMillis());
    	taxiService.setPickupTime(timeNow);
    	Assertions.assertEquals(timeNow, taxiService.getPickupTime());
    }
    
    @Test
    public void shouldSetArrivalTime() {
    	Timestamp timeNow = new Timestamp(System.currentTimeMillis());
    	taxiService.setArrivalTime(timeNow);
    	Assertions.assertEquals(timeNow, taxiService.getArrivalTime());
    }
    
    @Test
    public void shouldSetDistance() {
    	taxiService.setDistanceKM(8.5);
    	Assertions.assertEquals(8.5, taxiService.getDistanceKM());
    }
    
    @Test
    public void shouldSetPrice() {
    	taxiService.setPrice(50.50);
    	Assertions.assertEquals(50.50, taxiService.getPrice());
    }
    
    @Test
    public void shouldSetRating() {
    	taxiService.setServiceRating(6.2);
    	Assertions.assertEquals(6.2, taxiService.getServiceRating());
    }
    
    @Test
    public void shouldValidateID() {
    	Assertions.assertThrows(ValidationException.class, () -> taxiService.validateID(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateID(0L));
    }

    @Test
    public void shouldValidateDriverForeignKey() {
    	taxiDriverFK = new TaxiDriver(0);
        taxiService.setDriverFK(taxiDriverFK);
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateDriverFK(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateDriverFK(taxiService.getDriverFK()));
    }

    @Test
    public void shouldValidateCustomerForeignKey() {
        customerFK = new Customer(0);
        taxiService.setCustomerFK(customerFK);
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateCustomerFK(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateCustomerFK(taxiService.getCustomerFK()));
    }

    @Test
    public void shouldValidatePickupLocation() {
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validatePickupLocation(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validatePickupLocation("ab"));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validatePickupLocation("        "));
    }

    @Test
    public void shouldValidateDestination() {
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateDestination(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateDestination("ab"));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateDestination("        "));
    }

    @Test
    public void shouldValidatePickupTime() {
        Timestamp pickupTime = new Timestamp(new Date().getTime());
        pickupTime.setTime(pickupTime.getTime() + TimeUnit.HOURS.toHours(2));
        Timestamp arrivalTime = new Timestamp(new Date().getTime());

        taxiService.setPickupTime(pickupTime);
        taxiService.setArrivalTime(arrivalTime);

        Assertions.assertThrows(ValidationException.class, () -> taxiService.validatePickupTime(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validatePickupTime(taxiService.getPickupTime()));
    }

    @Test
    public void shouldValidateArrivalTime() {
        Timestamp pickupTime = new Timestamp(new Date().getTime());
        pickupTime.setTime(pickupTime.getTime() + TimeUnit.HOURS.toHours(5));
        Timestamp arrivalTime = new Timestamp(new Date().getTime());

        taxiService.setPickupTime(pickupTime);
        taxiService.setArrivalTime(arrivalTime);

        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateArrivalTime(null));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateArrivalTime(taxiService.getArrivalTime()));
    }

    @Test
    public void shouldValidateDistance() {
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateDistance(0.0));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateDistance(-2.0));
    }

    @Test
    public void shouldValidatePrice(){
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validatePrice(0.0));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validatePrice(-2.0));
    }

    @Test
    public void shouldValidateRating() {
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateRating(-0.1));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateRating(10.1));
        Assertions.assertThrows(ValidationException.class, () -> taxiService.validateRating(50.0));
    }

}
