package cl.taxi.api.models;

import cl.taxi.api.constants.Constant;
import cl.taxi.api.dto.VehicleDTO;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @NotNull(message = "The vehicle's brand is required.")
    @Size(min = 2, max = 128, message = "The vehicle's band requires between 2 and 128 characters.")
    @Column(name = "BRAND")
    private String vehicleBrand;

    @NotNull(message = "The vehicle's model is required.")
    @Size(min = 2, max = 128, message = "The vehicle's model requires between 2 and 128 characters.")
    @Column(name = "MODEL")
    private String vehicleModel;

    @NotNull(message = "The vehicle's color is required.")
    @Size(min = 2, max = 128, message = "The vehicle's color requires between 2 and 128 characters.")
    @Column(name = "COLOR")
    private String vehicleColor;

    @NotNull(message = "The vehicle's ammount of seats is required.")
    @Min(value = 2, message = "The vehicle must have at least 2 seats.")
    @Column(name = "SEATS_AMMOUNT")
    private int vehicleSeats;

    @NotNull(message = "The vehicle's registration date is required.")
    @Column(name = "REGISTRATION_DATE")
    private Date registrationDate;

    @NotNull(message = "The vehicle's license plate is required.")
    @Column(name = "LICENSE_PLATE", unique = true)
    private String licensePlate;

    @NotNull(message = "The car's status is required.")
    @Column(name = "ACTIVE")
    private Boolean isActive;

    // Constructor

    public Vehicle() { }

    public Vehicle(VehicleDTO vehicle) {
    	this.id = vehicle.getID();
        this.vehicleBrand = vehicle.getBrand();
        this.vehicleModel = vehicle.getModel();
        this.vehicleColor = vehicle.getColor();
        this.vehicleSeats = vehicle.getSeats();
        this.registrationDate = vehicle.getRegistrationDate();
        this.licensePlate = vehicle.getLicensePlate();
        this.isActive = vehicle.isActive();
    }

    // Getters

    public long getID() {
        return id;
    }

    public String getVehicleBrand() {
        return vehicleBrand;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    public int getVehicleSeats() {
        return vehicleSeats;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public Boolean isActive() {
        return isActive;
    }

    // Setters

    public void setID(long id) {
        this.id = id;
    }

    public void setVehicleBrand(String vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public void setVehicleSeats(int vehicleSeats) {
        this.vehicleSeats = vehicleSeats;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    // Business Logic
    
    public void validateID(Long id) {
    	if(id == null) {
    		throw new ValidationException("The vehicle's id cannot be null.");
    	}
    	else if(id < 1) {
    		throw new ValidationException("The vehicle's id must be 1 or higher.");
    	}
    }

    public void validateBrand(String brand) {
        if(brand == null) {
            throw new ValidationException("The vehicle's brand is required.");
        }
        else if(!brand.matches(Constant.ALPHANUMERIC_REGEXP)) {
            throw new ValidationException("The vehicle's brand only allows alphanumeric characters.");
        }
        else if(brand.trim().length() < 2) {
            throw new ValidationException("The vehicle's brand must contain at least 2 characters.");
        }
        else if(brand.trim().length() > 128) {
            throw new ValidationException("The vehicle's brand allows a maximum of 128 characters.");
        }
    }

    public void validateModel(String model) {
        if(model == null) {
            throw new ValidationException("The vehicle's model is required.");
        }
        else if(!model.matches(Constant.ALPHANUMERIC_REGEXP)) {
            throw new ValidationException("The vehicle's model only allows alphanumeric characters.");
        }
        else if(model.trim().length() < 2) {
            throw new ValidationException("The vehicle's model must contain at least 2 characters.");
        }
        else if(model.trim().length() > 128) {
            throw new ValidationException("The vehicle's model allows a maximum of 128 characters.");
        }
    }

    public void validateColor(String color) {
        if(color == null) {
            throw new ValidationException("The vehicle's color is required.");
        }
        else if(!color.matches("[a-zA-Z0-9]*")) {
            throw new ValidationException("The vehicle's color only allows alphanumeric characters.");
        }
        else if(color.trim().length() < 2) {
            throw new ValidationException("The vehicle's color must contain at least 2 characters.");
        }
        else if(color.trim().length() > 128) {
            throw new ValidationException("The vehicle's color allows a maximum of 128 characters.");
        }
    }

    public void validateSeats(Integer seats) {
        if(seats == null) {
            throw new ValidationException("The vehicle's ammount of seats are required.");
        }
        if(seats < 2) {
            throw new ValidationException("The vehicle must have at least 2 seats.");
        }
    }

    public void validateLicensePlate(String plate) {
        if(plate == null) {
            throw new ValidationException("The vehicle's plate is required.");
        }
        else if(!plate.matches("[a-zA-Z0-9-]*")) {
            throw new ValidationException("The vehicle's plate only allows alphanumeric characters.");
        }
        else if(plate.trim().length() < 4) {
            throw new ValidationException("The vehicle's plate must contain at least 4 characters.");
        }
        else if(plate.trim().length() > 128) {
            throw new ValidationException("The vehicle's plate allows a maximum of 128 characters.");
        }
    }

    public void validateActive(Boolean active) {
        if(active == null) {
            throw new ValidationException("The vehicle's status is required.");
        }
    }

}
