package cl.taxi.api.models;

import cl.taxi.api.dto.TaxiServiceDTO;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Table(name = "TAXI_SERVICE")
@Entity
public class TaxiService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @NotNull(message = "The time at which the order was requested is required.")
    @Column(name = "ORDER_TIME")
    private Timestamp orderTime = new Timestamp(System.currentTimeMillis());

    @NotNull(message = "The driver's foreign key is required.")
    @ManyToOne()
    @JoinColumn(name = "DRIVER_FK")
    private TaxiDriver driverFK;

    @NotNull(message = "The customer's foreign key is required.")
    @ManyToOne()
    @JoinColumn(name = "CUSTOMER_FK")
    private Customer customerFK;

    @NotNull(message = "The customer's pickup location is required.")
    @Column(name = "PICKUP_LOCATION")
    private String pickupLocation;

    @NotNull(message = "The customer's destination is required.")
    @Column(name = "DESTINATION")
    private String destination;

    @DateTimeFormat(pattern="DD/MM/YYYY HH:mm")
    @Column(name = "PICKUP_TIME")
    private Timestamp pickupTime;

    @DateTimeFormat(pattern="DD/MM/YYYY HH:mm")
    @Column(name = "ARRIVAL_TIME")
    private Timestamp arrivalTime;

    @Min(value = 0, message = "The minimum distance required is 0.1 kilometers.")
    @Column(name = "DISTANCE_KM")
    private Double distanceKM = null;

    @Column(name = "PRICE")
    private Double price = null;

    @Column(name = "SERVICE_RATING")
    private Double serviceRating = null;

    // Constructors

    public TaxiService() { }

    public TaxiService(TaxiServiceDTO dto) {
        this.id = dto.getID();
        this.orderTime = dto.getOrderTime();
        this.driverFK = dto.getDriver();
        this.customerFK = dto.getCustomer();
        this.pickupLocation = dto.getPickupLocation();
        this.destination = dto.getDestination();
        this.pickupTime = dto.getPickup();
        this.arrivalTime = dto.getArrival();
        this.distanceKM = dto.getKilometers();
        this.price = dto.getPrice();
        this.serviceRating = dto.getServiceRating();
    }

    // Getters

    public long getID() {
        return id;
    }

    public Timestamp getOrderTime() {
        return orderTime;
    }

    public TaxiDriver getDriverFK() {
        return driverFK;
    }

    public Customer getCustomerFK() {
        return customerFK;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public String getDestination() {
        return destination;
    }

    public Timestamp getPickupTime() {
        return pickupTime;
    }

    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    public Double getDistanceKM() {
        return distanceKM;
    }

    public Double getPrice() {
        return price;
    }

    public Double getServiceRating() {
        return serviceRating;
    }

    // Setters

    public void setID(long id) {
        this.id = id;
    }

    public void setOrderTime(Timestamp orderTime) {
        this.orderTime = orderTime;
    }

    public void setDriverFK(TaxiDriver driverFK) {
        this.driverFK = driverFK;
    }

    public void setCustomerFK(Customer customerFK) {
        this.customerFK = customerFK;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setPickupTime(Timestamp pickupTime) {
        this.pickupTime = pickupTime;
    }

    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public void setDistanceKM(Double distanceKM) {
        this.distanceKM = distanceKM;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setServiceRating(Double serviceRating) {
        this.serviceRating = serviceRating;
    }

    // Business logic
    
    public void validateID(Long id) {
    	if(id == null) {
    		throw new ValidationException("The taxi service's id cannot be null.");
    	}
    	else if(!id.toString().matches("[0-9]+")) {
            throw new ValidationException("The taxi service's id can only contain numbers.");
        }
    	else if(id < 1) {
    		throw new ValidationException("The taxi service's id must be 1 or higher.");
    	}
    }

    public void validateDriverFK(TaxiDriver driverFK) {
        if(driverFK == null) {
            throw new ValidationException("The driver's foreign key cannot be null.");
        }
        else if(driverFK.getID() == 0) {
            throw new ValidationException("The driver's foreign key ID is required.");
        }
    }

    public void validateCustomerFK(Customer customerFK) {
        if(customerFK == null) {
            throw new ValidationException("The customer's foreign key cannot be null.");
        }
        else if(customerFK.getID() == 0) {
            throw new ValidationException("The customer's foreign key ID is required.");
        }
    }

    public void validatePickupLocation(String location) {
        if(location == null) {
            throw new ValidationException("The service's location cannot be null.");
        }
        else if(location.trim().length() < 3) {
            throw new ValidationException("The service's location cannot contain fewer than 3 characters.");
        }
    }

    public void validateDestination(String destination) {
        if(destination == null) {
            throw new ValidationException("The service's destination cannot be null.");
        }
        else if(destination.trim().length() < 3) {
            throw new ValidationException("The service's destination cannot contain fewer than 3 characters.");
        }
    }

    public void validatePickupTime(Timestamp time) {
        if(time == null) {
            throw new ValidationException("The pickup's time cannot be null.");
        } else if (time.after(this.arrivalTime)) {
            throw new ValidationException("Pickup's time cannot be after arrival's time.");
        }
    }

    public void validateArrivalTime(Timestamp time) {
        if(time == null) {
            throw new ValidationException("The arrival's time cannot be null.");
        }
        else if(time.before(this.pickupTime)) {
            throw new ValidationException("The arrival's time cannot be earlier than the pickup time.");
        }
    }

    public void validateDistance(Double distance) {
        if(distance != null && distance < 0.1) {
            throw new ValidationException("The service's distance cannot be below 0.1 kilometers.");
        }
    }

    public void validatePrice(Double price) {
        if(price != null && price < 0.1) {
            throw new ValidationException("The service's price cannot be below 0.1");
        }
    }

    public void validateRating(Double rating) {
       if(rating != null) {
           if(!rating.toString().matches("[0-9]?[0-9]?(\\.[0-9][0-9]?)?")) {
               throw new ValidationException("The service's rating only allows numbers.");
           }
           else if(rating < 0.0 || rating > 10.0) {
               throw new ValidationException("The service's rating must be between 0.0 and 10.0");
           }
       }
    }
}
