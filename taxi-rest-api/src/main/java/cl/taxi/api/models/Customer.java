package cl.taxi.api.models;

import cl.taxi.api.dto.CustomerDTO;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;

@Table(name = "Customer")
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Size(min = 2, max = 128, message = "The firstname field requires between 2 and 128 characters.")
    @Column(name = "FIRSTNAME")
    private String firstName;

    @Size(min = 2, max = 128, message = "The lastname field requires between 2 and 128 characters.")
    @Column(name = "LASTNAME")
    private String lastName;

    @Min(value = 0, message = "The minimum rating is 0.0")
    @Max(value = 10, message = "The maximum rating is 10.")
    @Column(name = "USER_RATING")
    private double userRating;

    @Column(name = "JOIN_DATE")
    private Date joinDate;

    @Size(min = 6, max = 50, message = "The phone number field requires between 6 and 50 characters.")
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Size(min = 6, max = 72, message = "The email field requires between 6 and 72 characters.")
    @Column(name = "EMAIL")
    private String email;

    // Constructors

    public Customer() { }

    public Customer(long id) {
        this.id = id;
    }

    public Customer(CustomerDTO dto) {
        this.id = dto.getID();
        this.firstName = dto.getName();
        this.lastName = dto.getLastname();
        this.userRating = dto.getRating();
        this.joinDate = dto.getJoinDate();
        this.phoneNumber = dto.getPhone();
        this.email = dto.getEmail();
    }

    // Getters

    public long getID() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getUserRating() {
        return userRating;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    // Setters

    public void setID(long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUserRating(double userRating) {
        this.userRating = userRating;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // Business Logic
    
    public void validateID(Long id) {
    	if(id == null) {
    		throw new ValidationException("The customer's id cannot be null.");
    	}
    	else if(id < 1) {
    		throw new ValidationException("The customer's id must be 1 or higher.");
    	}
    }

    public void validateFirstName(String name) {
        if(name == null) {
            throw new ValidationException("The customer's firstname is required.");
        }
        else if(!name.matches("^[ A-Za-z]+$")) {
            throw new ValidationException("The customer's name can only contain letters.");
        }
        else if(name.trim().length() < 2) {
            throw new ValidationException("The customer's name must requires at least 2 letters.");
        }
        else if(name.trim().length() > 128) {
            throw new ValidationException("The customer's name allows a maximum of 128 characters.");
        }
    }

    public void validateLastName(String lastName) {
        if(lastName == null) {
            throw new ValidationException("The customer's lastname is required.");
        }
        else if(!lastName.matches("^[ A-Za-z]+$")) {
            throw new ValidationException("The customer's lastname can only contain letters.");
        }
        else if(lastName.trim().length() < 2) {
            throw new ValidationException("The customer's lastname requires at least 2 letters.");
        }
        else if(lastName.trim().length() > 128) {
            throw new ValidationException("The customer's lastname allows a maximum of 128 characters.");
        }
    }

    public void validateRating(Double rating) {
        if(rating == null) {
            throw new ValidationException("The customer's rating cannot be null.");
        }
        if(!rating.toString().matches("[0-9]?[0-9]?(\\.[0-9][0-9]?)?")) {
           throw new ValidationException("The customer's rating only allows numbers.");
        }
        else if(rating < 0.0 || rating > 10.0) {
            throw new ValidationException("The customer's rating must be between 0.0 and 10.0");
        }
    }

    public void validatePhoneNumber(String number) {
        if(number == null) {
            throw new ValidationException("The customer's number is required.");
        }
        else if(number.length() < 6) {
            throw new ValidationException("The customer's number requires at least 6 characters.");
        }
        if(!number.matches(".*\\d.*")) {
            throw new ValidationException("The customer's phone number must contain numbers.");
        }
    }

    public void validateEmail(String email) {
        if(email == null) {
            throw new ValidationException("The customer's email is required.");
        }
        else if(email.trim().length() < 4) {
            throw new ValidationException("The customer's email must contain at least 4 characters.");
        }
        else if(email.trim().length() > 50) {
            throw new ValidationException("The customer's email cannot contain over 50 characters.");
        }
        else if(!email.contains("@")) {
            throw new ValidationException("The customer's email must contain the '@' character.");
        }
    }

}
