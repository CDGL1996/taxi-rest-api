package cl.taxi.api.models;

import cl.taxi.api.dto.TaxiDriverDTO;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;

@Table(name = "TAXI_DRIVER")
@Entity
public class TaxiDriver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Size(min = 2, max = 128, message = "The firstname field requires between 2 and 128 characters.")
    @Column(name = "FIRSTNAME")
    private String firstName;

    @Size(min = 2, max = 128, message = "The lastname field requires between 2 and 128 characters.")
    @Column(name = "LASTNAME")
    private String lastName;

    @Min(value = 0, message = "The minimum rating is 0.0")
    @Max(value = 10, message = "The maximum rating is 10.")
    @Column(name = "DRIVER_RATING")
    private double driverRating;

    @Column(name = "JOIN_DATE")
    private Date joinDate;

    @Size(min = 6, max = 50, message = "The phone number field requires between 6 and 50 characters.")
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Min(value = 0, message = "The driver's jobs value must be at minimum at 0.")
    @Column(name = "JOBS_PERFORMED")
    private int jobsPerformed;

    @ManyToOne()
    @JoinColumn(name = "VEHICLE_FK")
    private Vehicle vehicleFK;

    // Constructors

    public TaxiDriver() { }

    public TaxiDriver(long id) {
        this.id = id;
    }

    public TaxiDriver(TaxiDriverDTO dto) {
        this.id = dto.getID();
        this.firstName = dto.getName();
        this.lastName = dto.getLastname();
        this.driverRating = dto.getRating();
        this.joinDate = dto.getJoinDate();
        this.phoneNumber = dto.getPhone();
        this.jobsPerformed = dto.getJobsDone();
        this.vehicleFK = dto.getVehicle();
    }

    // Getters

    public long getID() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getDriverRating() {
        return driverRating;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getJobsPerformed() {
        return jobsPerformed;
    }

    public Vehicle getVehicleFK() {
        return vehicleFK;
    }

    // Setters

    public void setID(long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDriverRating(double driverRating) {
        this.driverRating = driverRating;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setJobsPerformed(int jobsPerformed) {
        this.jobsPerformed = jobsPerformed;
    }

    public void setVehicleFK(Vehicle vehicleFK) {
        this.vehicleFK = vehicleFK;
    }

    // Business Logic
    
    public void validateID(Long id) {
    	if(id == null) {
    		throw new ValidationException("The taxi driver's id cannot be null.");
    	}
    	else if(id < 1) {
    		throw new ValidationException("The taxi driver's id must be 1 or higher.");
    	}
    }

    public void validateFirstName(String name) {
        if(name == null) {
            throw new ValidationException("The taxi driver's firstname is required.");
        }
        else if(!name.matches("^[ A-Za-z]+$")) {
            throw new ValidationException("The taxi driver's name can only contain letters.");
        }
        else if(name.trim().length() < 2) {
            throw new ValidationException("The taxi driver's name must requires at least 2 letters.");
        }
        else if(name.trim().length() > 128) {
            throw new ValidationException("The taxi driver's name allows a maximum of 128 characters.");
        }
    }

    public void validateLastName(String lastName) {
        if(lastName == null) {
            throw new ValidationException("The taxi driver's lastname is required.");
        }
        else if(!lastName.matches("^[ A-Za-z]+$")) {
            throw new ValidationException("The taxi driver's lastname can only contain letters.");
        }
        else if(lastName.trim().length() < 2) {
            throw new ValidationException("The taxi driver's lastname requires at least 2 letters.");
        }
        else if(lastName.trim().length() > 128) {
            throw new ValidationException("The taxi driver's lastname allows a maximum of 128 characters.");
        }
    }

    public void validateRating(Double rating) {
        if(rating == null) {
            throw new ValidationException("The taxi driver's rating cannot be null.");
        }
        if(!rating.toString().matches("[0-9]?[0-9]?(\\.[0-9][0-9]?)?")) {
            throw new ValidationException("The taxi driver's rating only allows numbers.");
        }
        else if(rating < 0.0 || rating > 10.0) {
            throw new ValidationException("The taxi driver's rating must be between 0.0 and 10.0");
        }
    }

    public void validatePhoneNumber(String number) {
        if(number == null) {
            throw new ValidationException("The taxi driver's number is required.");
        }
        else if(number.length() < 6) {
            throw new ValidationException("The taxi driver's number requires at least 6 characters.");
        }
        if(!number.matches(".*\\d.*")) {
            throw new ValidationException("The taxi driver's phone number must contain numbers.");
        }
    }

    public void validateJobs(Integer jobsPerformed) {
        if(jobsPerformed == null) {
            throw new ValidationException("The ammount of jobs performed is required.");
        }
        else if(jobsPerformed < 0) {
            throw new ValidationException("The minimum ammount of jobs required is 0.");
        }
    }

    public void validateVehicle(Vehicle vehicle) {
        if(vehicle == null) {
            throw new ValidationException("The vehicle's foreign key cannot be null.");
        }
        else if(vehicle.getID() < 1) {
            throw new ValidationException("The vehicle's foreign key ID must be 1 or higher..");
        }
    }

}
