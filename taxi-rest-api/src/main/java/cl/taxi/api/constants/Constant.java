package cl.taxi.api.constants;

public final class Constant {
	
	public static final String LOCATION_HEADER = "location";
	public static final String ALPHANUMERIC_REGEXP = "[a-zA-Z0-9]*";
	
	public static final String SQL_EXCEPTION = "SQL Exception: ";
	
	private Constant() { }

}
