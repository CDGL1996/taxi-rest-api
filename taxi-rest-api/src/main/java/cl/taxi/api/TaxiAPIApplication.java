package cl.taxi.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaxiAPIApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaxiAPIApplication.class, args);
	}

}
