package cl.taxi.api.dao;

import cl.taxi.api.models.TaxiService;

import java.util.Date;
import java.util.List;

public interface TaxiServiceDAO {

    List<TaxiService> getTaxiServices(String driverName, String driverLastName);
    List<TaxiService> getTaxiServices(Date date);
    TaxiService getTaxiService(long id);
    TaxiService addTaxiService(TaxiService taxiService);
    TaxiService updateTaxiService(long id, TaxiService taxiService);
    TaxiService deleteTaxiService(long id);

}
