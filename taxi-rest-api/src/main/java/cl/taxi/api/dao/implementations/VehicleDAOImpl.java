package cl.taxi.api.dao.implementations;

import cl.taxi.api.configuration.DBConfig;
import cl.taxi.api.constants.Constant;
import cl.taxi.api.dao.VehicleDAO;
import cl.taxi.api.models.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class VehicleDAOImpl implements VehicleDAO {

    private static final Logger LOGGER = Logger.getLogger(VehicleDAOImpl.class.getName());

    @Autowired
    private DBConfig dbConfig;

    @Override
    public List<Vehicle> getAllVehicles() {

        List<Vehicle> vehiclesList = new ArrayList<>();

        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

                PreparedStatement ps = c.prepareStatement("SELECT " +
                        "ID, BRAND, MODEL, COLOR, SEATS_AMMOUNT, REGISTRATION_DATE, LICENSE_PLATE, ACTIVE " +
                        "FROM VEHICLE"))
        {
            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    Vehicle vehicle = new Vehicle();
                    vehicle.setID(rs.getLong("ID"));
                    vehicle.setVehicleBrand(rs.getString("BRAND"));
                    vehicle.setVehicleModel(rs.getString("MODEL"));
                    vehicle.setVehicleColor(rs.getString("COLOR"));
                    vehicle.setVehicleSeats(rs.getInt("SEATS_AMMOUNT"));
                    vehicle.setRegistrationDate(rs.getDate("REGISTRATION_DATE"));
                    vehicle.setLicensePlate(rs.getString("LICENSE_PLATE"));
                    vehicle.setActive(rs.getBoolean("ACTIVE"));
                    vehiclesList.add(vehicle);
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return vehiclesList;
    }

    @Override
    public Vehicle getVehicle(long id) {
        Vehicle vehicleFound = new Vehicle();

        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, BRAND, MODEL, COLOR, SEATS_AMMOUNT, REGISTRATION_DATE, LICENSE_PLATE, ACTIVE " +
                    "FROM VEHICLE WHERE ID = ?"))
        {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    vehicleFound.setID(rs.getLong("ID"));
                    vehicleFound.setVehicleBrand(rs.getString("BRAND"));
                    vehicleFound.setVehicleModel(rs.getString("MODEL"));
                    vehicleFound.setVehicleColor(rs.getString("COLOR"));
                    vehicleFound.setVehicleSeats(rs.getInt("SEATS_AMMOUNT"));
                    vehicleFound.setRegistrationDate(rs.getDate("REGISTRATION_DATE"));
                    vehicleFound.setLicensePlate(rs.getString("LICENSE_PLATE"));
                    vehicleFound.setActive(rs.getBoolean("ACTIVE"));
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return vehicleFound;
    }

    @Override
    public Vehicle getVehicle(String plateNumber) {

        Vehicle vehicleFound = new Vehicle();
        String vehiclePlate = plateNumber.toUpperCase().trim();

        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, BRAND, MODEL, COLOR, SEATS_AMMOUNT, REGISTRATION_DATE, LICENSE_PLATE, ACTIVE " +
                    "FROM VEHICLE WHERE UPPER(LICENSE_PLATE) = ?"))
        {

            ps.setString(1, vehiclePlate);

            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    vehicleFound.setID(rs.getLong("ID"));
                    vehicleFound.setVehicleBrand(rs.getString("BRAND"));
                    vehicleFound.setVehicleModel(rs.getString("MODEL"));
                    vehicleFound.setVehicleColor(rs.getString("COLOR"));
                    vehicleFound.setVehicleSeats(rs.getInt("SEATS_AMMOUNT"));
                    vehicleFound.setRegistrationDate(rs.getDate("REGISTRATION_DATE"));
                    vehicleFound.setLicensePlate(rs.getString("LICENSE_PLATE"));
                    vehicleFound.setActive(rs.getBoolean("ACTIVE"));
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return vehicleFound;
    }

    @Override
    public Vehicle addVehicle(Vehicle vehicle) {
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("INSERT INTO VEHICLE " +
                    "(BRAND, MODEL, COLOR, SEATS_AMMOUNT, REGISTRATION_DATE, LICENSE_PLATE, ACTIVE) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)")) {

                c.setAutoCommit(false);
                Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
                vehicle.setRegistrationDate(currentDate);

                ps.setString(1, vehicle.getVehicleBrand().trim());
                ps.setString(2, vehicle.getVehicleModel().trim());
                ps.setString(3, vehicle.getVehicleColor().trim());
                ps.setInt(4, vehicle.getVehicleSeats());
                ps.setDate(5, currentDate);
                ps.setString(6, vehicle.getLicensePlate().toUpperCase().trim());
                ps.setBoolean(7, vehicle.isActive());

                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

            try(PreparedStatement lastID = c.prepareStatement("SELECT ID FROM VEHICLE ORDER BY ID DESC LIMIT 1");
                ResultSet rsID = lastID.executeQuery()
            ) {
                while(rsID.next()) {
                    vehicle.setID(rsID.getLong("ID"));
                }
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return vehicle;
    }

    @Override
    public Vehicle updateVehicle(long id, Vehicle vehicle) {

        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("UPDATE VEHICLE " +
                    "SET BRAND = ?, MODEL = ?, COLOR = ?, SEATS_AMMOUNT = ?, " +
                    "LICENSE_PLATE = ?, ACTIVE = ? " +
                    "WHERE ID = ?")) {

                c.setAutoCommit(false);

                ps.setString(1, vehicle.getVehicleBrand().trim());
                ps.setString(2, vehicle.getVehicleModel().trim());
                ps.setString(3, vehicle.getVehicleColor().trim());
                ps.setInt(4, vehicle.getVehicleSeats());
                ps.setString(5, vehicle.getLicensePlate().toUpperCase().trim());
                ps.setBoolean(6, vehicle.isActive());
                ps.setLong(7, id);

                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return getVehicle(id);
    }

    @Override
    public Vehicle deleteVehicle(long id) {

        Vehicle deletedVehicle = getVehicle(id);
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("DELETE FROM VEHICLE " +
                    "WHERE ID = ?")) {

                c.setAutoCommit(false);
                ps.setLong(1, id);
                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return deletedVehicle;
    }

}
