package cl.taxi.api.dao.implementations;

import cl.taxi.api.configuration.DBConfig;
import cl.taxi.api.constants.Constant;
import cl.taxi.api.dao.CustomerDAO;
import cl.taxi.api.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

    private static final Logger LOGGER = Logger.getLogger(CustomerDAOImpl.class.getName());

    @Autowired
    private DBConfig dbConfig;

    @Override
    public List<Customer> getAllCustomers() {
        List<Customer> customersList = new ArrayList<>();
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, FIRSTNAME, LASTNAME, USER_RATING, JOIN_DATE, PHONE_NUMBER, EMAIL " +
                    "FROM CUSTOMER"))
        {
            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    Customer customer = new Customer();
                    customer.setID(rs.getLong("ID"));
                    customer.setFirstName(rs.getString("FIRSTNAME"));
                    customer.setLastName(rs.getString("LASTNAME"));
                    customer.setUserRating(rs.getDouble("USER_RATING"));
                    customer.setJoinDate(rs.getDate("JOIN_DATE"));
                    customer.setPhoneNumber(rs.getString("PHONE_NUMBER"));
                    customer.setEmail(rs.getString("EMAIL"));
                    customersList.add(customer);
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return customersList;
    }

    @Override
    public Customer getCustomer(long id) {
        Customer customerFound = new Customer();
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, FIRSTNAME, LASTNAME, USER_RATING, JOIN_DATE, PHONE_NUMBER, EMAIL " +
                    "FROM CUSTOMER WHERE ID = ?"))
        {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    customerFound.setID(rs.getLong("ID"));
                    customerFound.setFirstName(rs.getString("FIRSTNAME"));
                    customerFound.setLastName(rs.getString("LASTNAME"));
                    customerFound.setUserRating(rs.getDouble("USER_RATING"));
                    customerFound.setJoinDate(rs.getDate("JOIN_DATE"));
                    customerFound.setPhoneNumber(rs.getString("PHONE_NUMBER"));
                    customerFound.setEmail(rs.getString("EMAIL"));
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return customerFound;
    }

    @Override
    public Customer getCustomer(String email) {
        Customer customerFound = new Customer();
        String customerEmail = email.toUpperCase().trim();

        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, FIRSTNAME, LASTNAME, USER_RATING, JOIN_DATE, PHONE_NUMBER, EMAIL " +
                    "FROM CUSTOMER WHERE UPPER(EMAIL) = ?"))
        {

            ps.setString(1, customerEmail);

            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    customerFound.setID(rs.getLong("ID"));
                    customerFound.setFirstName(rs.getString("FIRSTNAME"));
                    customerFound.setLastName(rs.getString("LASTNAME"));
                    customerFound.setUserRating(rs.getDouble("USER_RATING"));
                    customerFound.setJoinDate(rs.getDate("JOIN_DATE"));
                    customerFound.setPhoneNumber(rs.getString("PHONE_NUMBER"));
                    customerFound.setEmail(rs.getString("EMAIL"));
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return customerFound;
    }

    @Override
    public Customer addCustomer(Customer customer) {
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("INSERT INTO CUSTOMER " +
                    "(FIRSTNAME, LASTNAME, USER_RATING, JOIN_DATE, PHONE_NUMBER, EMAIL) " +
                    "VALUES (?, ?, ?, ?, ?, ?)")) {

                c.setAutoCommit(false);
                Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
                customer.setJoinDate(currentDate);

                ps.setString(1, customer.getFirstName().trim());
                ps.setString(2, customer.getLastName().trim());
                ps.setDouble(3, customer.getUserRating());
                ps.setDate(4, currentDate);
                ps.setString(5, customer.getPhoneNumber().trim());
                ps.setString(6, customer.getEmail().trim());

                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

            try(PreparedStatement lastID = c.prepareStatement("SELECT ID FROM CUSTOMER ORDER BY ID DESC LIMIT 1");
                ResultSet rsID = lastID.executeQuery()
            ) {
                while(rsID.next()) {
                    customer.setID(rsID.getLong("ID"));
                }
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return customer;
    }

    @Override
    public Customer updateCustomer(long id, Customer customer) {
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("UPDATE CUSTOMER " +
                    "SET FIRSTNAME = ?, LASTNAME = ?, USER_RATING = ?, " +
                    "PHONE_NUMBER = ?, EMAIL = ? " +
                    "WHERE ID = ?")) {

                c.setAutoCommit(false);

                ps.setString(1, customer.getFirstName().trim());
                ps.setString(2, customer.getLastName().trim());
                ps.setDouble(3, customer.getUserRating());
                ps.setString(4, customer.getPhoneNumber().toUpperCase().trim());
                ps.setString(5, customer.getEmail());
                ps.setLong(6, id);

                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return getCustomer(id);
    }

    @Override
    public Customer deleteCustomer(long id) {
        Customer deletedCustomer = getCustomer(id);
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("DELETE FROM CUSTOMER " +
                    "WHERE ID = ?")) {

                c.setAutoCommit(false);
                ps.setLong(1, id);
                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return deletedCustomer;
    }

}
