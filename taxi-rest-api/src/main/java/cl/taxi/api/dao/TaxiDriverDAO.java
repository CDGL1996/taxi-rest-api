package cl.taxi.api.dao;

import cl.taxi.api.models.TaxiDriver;

import java.util.List;

public interface TaxiDriverDAO {

    List<TaxiDriver> getAllTaxiDrivers();
    TaxiDriver getTaxiDriver(long id);
    TaxiDriver getTaxiDriver(String firstName, String lastName);
    TaxiDriver addTaxiDriver(TaxiDriver taxiDriver);
    TaxiDriver updateTaxiDriver(long id, TaxiDriver taxiDriver);
    TaxiDriver deleteTaxiDriver(long id);

}
