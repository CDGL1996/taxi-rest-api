package cl.taxi.api.dao;

import cl.taxi.api.models.Vehicle;

import java.sql.SQLException;
import java.util.List;

public interface VehicleDAO {

    List<Vehicle> getAllVehicles() throws SQLException;
    Vehicle getVehicle(long id);
    Vehicle getVehicle(String plateNumber);
    Vehicle addVehicle(Vehicle vehicle);
    Vehicle updateVehicle(long id, Vehicle vehicle);
    Vehicle deleteVehicle(long id);

}
