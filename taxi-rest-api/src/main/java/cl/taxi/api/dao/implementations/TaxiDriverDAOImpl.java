package cl.taxi.api.dao.implementations;

import cl.taxi.api.configuration.DBConfig;
import cl.taxi.api.constants.Constant;
import cl.taxi.api.dao.TaxiDriverDAO;
import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class TaxiDriverDAOImpl implements TaxiDriverDAO {

    private static final Logger LOGGER = Logger.getLogger(TaxiDriverDAOImpl.class.getName());

    @Autowired
    private DBConfig dbConfig;

    @Override
    public List<TaxiDriver> getAllTaxiDrivers() {
        List<TaxiDriver> driversList = new ArrayList<>();
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, FIRSTNAME, LASTNAME, DRIVER_RATING, JOIN_DATE, PHONE_NUMBER, JOBS_PERFORMED, VEHICLE_FK " +
                    "FROM TAXI_DRIVER"))
        {
            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    TaxiDriver taxiDriver = new TaxiDriver();
                    taxiDriver.setID(rs.getLong("ID"));
                    taxiDriver.setFirstName(rs.getString("FIRSTNAME"));
                    taxiDriver.setLastName(rs.getString("LASTNAME"));
                    taxiDriver.setDriverRating(rs.getDouble("DRIVER_RATING"));
                    taxiDriver.setJoinDate(rs.getDate("JOIN_DATE"));
                    taxiDriver.setPhoneNumber(rs.getString("PHONE_NUMBER"));
                    taxiDriver.setJobsPerformed(rs.getInt("JOBS_PERFORMED"));
                    Vehicle vehicleFK = new Vehicle();
                    vehicleFK.setID(rs.getLong("VEHICLE_FK"));
                    taxiDriver.setVehicleFK(vehicleFK);
                    driversList.add(taxiDriver);
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return driversList;
    }

    @Override
    public TaxiDriver getTaxiDriver(long id) {
        TaxiDriver taxiDriverFound = new TaxiDriver();
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, FIRSTNAME, LASTNAME, DRIVER_RATING, JOIN_DATE, PHONE_NUMBER, JOBS_PERFORMED, VEHICLE_FK " +
                    "FROM TAXI_DRIVER WHERE ID = ?"))
        {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    taxiDriverFound.setID(rs.getLong("ID"));
                    taxiDriverFound.setFirstName(rs.getString("FIRSTNAME"));
                    taxiDriverFound.setLastName(rs.getString("LASTNAME"));
                    taxiDriverFound.setDriverRating(rs.getDouble("DRIVER_RATING"));
                    taxiDriverFound.setJoinDate(rs.getDate("JOIN_DATE"));
                    taxiDriverFound.setPhoneNumber(rs.getString("PHONE_NUMBER"));
                    taxiDriverFound.setJobsPerformed(rs.getInt("JOBS_PERFORMED"));
                    Vehicle vehicleFK = new Vehicle();
                    vehicleFK.setID(rs.getLong("VEHICLE_FK"));
                    taxiDriverFound.setVehicleFK(vehicleFK);
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return taxiDriverFound;
    }

    @Override
    public TaxiDriver getTaxiDriver(String firstName, String lastName) {
        TaxiDriver taxiDriverFound = new TaxiDriver();
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, FIRSTNAME, LASTNAME, DRIVER_RATING, JOIN_DATE, PHONE_NUMBER, JOBS_PERFORMED, VEHICLE_FK " +
                    "FROM TAXI_DRIVER WHERE UPPER(FIRSTNAME) = ? AND UPPER(LASTNAME) = ?"))
        {

            ps.setString(1, firstName.toUpperCase().trim());
            ps.setString(2, lastName.toUpperCase().trim());

            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    taxiDriverFound.setID(rs.getLong("ID"));
                    taxiDriverFound.setFirstName(rs.getString("FIRSTNAME"));
                    taxiDriverFound.setLastName(rs.getString("LASTNAME"));
                    taxiDriverFound.setDriverRating(rs.getDouble("DRIVER_RATING"));
                    taxiDriverFound.setJoinDate(rs.getDate("JOIN_DATE"));
                    taxiDriverFound.setPhoneNumber(rs.getString("PHONE_NUMBER"));
                    taxiDriverFound.setJobsPerformed(rs.getInt("JOBS_PERFORMED"));
                    Vehicle vehicleFK = new Vehicle();
                    vehicleFK.setID(rs.getLong("VEHICLE_FK"));
                    taxiDriverFound.setVehicleFK(vehicleFK);
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return taxiDriverFound;
    }

    @Override
    public TaxiDriver addTaxiDriver(TaxiDriver taxiDriver) {
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("INSERT INTO TAXI_DRIVER " +
                    "(FIRSTNAME, LASTNAME, DRIVER_RATING, JOIN_DATE, PHONE_NUMBER, JOBS_PERFORMED, VEHICLE_FK) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)")) {

                c.setAutoCommit(false);
                Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
                taxiDriver.setJoinDate(currentDate);

                ps.setString(1, taxiDriver.getFirstName().trim());
                ps.setString(2, taxiDriver.getLastName().trim());
                ps.setDouble(3, taxiDriver.getDriverRating());
                ps.setDate(4, currentDate);
                ps.setString(5, taxiDriver.getPhoneNumber().trim());
                ps.setInt(6, taxiDriver.getJobsPerformed());
                ps.setLong(7, taxiDriver.getVehicleFK().getID());

                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

            try(PreparedStatement lastID = c.prepareStatement("SELECT ID FROM TAXI_DRIVER ORDER BY ID DESC LIMIT 1");
                ResultSet rsID = lastID.executeQuery()
            ) {
                while(rsID.next()) {
                    taxiDriver.setID(rsID.getLong("ID"));
                }
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return taxiDriver;
    }

    @Override
    public TaxiDriver updateTaxiDriver(long id, TaxiDriver taxiDriver) {
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("UPDATE TAXI_DRIVER " +
                    "SET FIRSTNAME = ?, LASTNAME = ?, DRIVER_RATING = ?, " +
                    "PHONE_NUMBER = ?, JOBS_PERFORMED = ? , VEHICLE_FK = ?" +
                    "WHERE ID = ?")) {

                c.setAutoCommit(false);

                ps.setString(1, taxiDriver.getFirstName().trim());
                ps.setString(2, taxiDriver.getLastName().trim());
                ps.setDouble(3, taxiDriver.getDriverRating());
                ps.setString(4, taxiDriver.getPhoneNumber().toUpperCase().trim());
                ps.setLong(5, taxiDriver.getJobsPerformed());
                ps.setLong(6, taxiDriver.getVehicleFK().getID());
                ps.setLong(7, id);

                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return getTaxiDriver(id);
    }

    @Override
    public TaxiDriver deleteTaxiDriver(long id) {
        TaxiDriver deletedTaxiDriver = getTaxiDriver(id);
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("DELETE FROM TAXI_DRIVER " +
                    "WHERE ID = ?")) {

                c.setAutoCommit(false);
                ps.setLong(1, id);
                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return deletedTaxiDriver;
    }

}
