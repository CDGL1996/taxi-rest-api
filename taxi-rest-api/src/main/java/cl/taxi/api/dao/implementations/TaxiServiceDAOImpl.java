package cl.taxi.api.dao.implementations;

import cl.taxi.api.configuration.DBConfig;
import cl.taxi.api.constants.Constant;
import cl.taxi.api.dao.TaxiServiceDAO;

import cl.taxi.api.models.Customer;
import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.TaxiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class TaxiServiceDAOImpl implements TaxiServiceDAO {

    private static final Logger LOGGER = Logger.getLogger(TaxiServiceDAOImpl.class.getName());

    @Autowired
    private DBConfig dbConfig;

    @Override
    public List<TaxiService> getTaxiServices(String driverName, String driverLastName) {
        List<TaxiService> taxiServices = new ArrayList<>();
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT s.ID, s.ORDER_TIME, s.DRIVER_FK, s.CUSTOMER_FK, " +
                    "s.PICKUP_LOCATION, s.DESTINATION, s.PICKUP_TIME, " +
                    "s.ARRIVAL_TIME, s.DISTANCE_KM, s.PRICE, s.SERVICE_RATING " +
                    "FROM TAXI_SERVICE s" +
                    "INNER JOIN TAXI_DRIVER t ON t.ID = s.ID" +
                    "WHERE UPPER(t.FIRSTNAME) = ? AND UPPER(t.LASTNAME) = ?"))
        {

            ps.setString(1, driverName.toUpperCase().trim());
            ps.setString(2, driverLastName.toUpperCase().trim());

            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    TaxiService taxiService = new TaxiService();
                    taxiService.setID(rs.getLong("ID"));
                    taxiService.setOrderTime(rs.getTimestamp("ORDER_TIME"));
                    taxiService.getDriverFK().setID(rs.getLong("DRIVER_FK"));
                    taxiService.getCustomerFK().setID(rs.getLong("CUSTOMER_FK"));
                    taxiService.setPickupLocation(rs.getString("PICKUP_LOCATION"));
                    taxiService.setDestination(rs.getString("DESTINATION"));
                    taxiService.setPickupTime(rs.getTimestamp("PICKUP_TIME"));
                    taxiService.setArrivalTime(rs.getTimestamp("ARRIVAL_TIME"));
                    taxiService.setDistanceKM(rs.getDouble("DISTANCE_KM"));
                    taxiService.setPrice(rs.getDouble("PRICE"));
                    taxiService.setServiceRating(rs.getDouble("SERVICE_RATING"));
                    taxiServices.add(taxiService);
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return taxiServices;
    }

    @Override
    public List<TaxiService> getTaxiServices(Date date) {
        List<TaxiService> taxiServices = new ArrayList<>();
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, ORDER_TIME, DRIVER_FK, CUSTOMER_FK, PICKUP_LOCATION, DESTINATION, PICKUP_TIME, " +
                    "ARRIVAL_TIME, DISTANCE_KM, PRICE, SERVICE_RATING " +
                    "FROM TAXI_SERVICE WHERE DATE(PICKUP_TIME) = ?"))
        {

            ps.setDate(1, new java.sql.Date(date.getTime()));

            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    TaxiService taxiService = new TaxiService();
                    taxiService.setID(rs.getLong("ID"));
                    taxiService.setOrderTime(rs.getTimestamp("ORDER_TIME"));
                    taxiService.getDriverFK().setID(rs.getLong("DRIVER_FK"));
                    taxiService.getCustomerFK().setID(rs.getLong("CUSTOMER_FK"));
                    taxiService.setPickupLocation(rs.getString("PICKUP_LOCATION"));
                    taxiService.setDestination(rs.getString("DESTINATION"));
                    taxiService.setPickupTime(rs.getTimestamp("PICKUP_TIME"));
                    taxiService.setArrivalTime(rs.getTimestamp("ARRIVAL_TIME"));
                    taxiService.setDistanceKM(rs.getDouble("DISTANCE_KM"));
                    taxiService.setPrice(rs.getDouble("PRICE"));
                    taxiService.setServiceRating(rs.getDouble("SERVICE_RATING"));
                    taxiServices.add(taxiService);
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return taxiServices;
    }

    @Override
    public TaxiService getTaxiService(long id) {
        TaxiService taxiService = new TaxiService();

        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

            PreparedStatement ps = c.prepareStatement("SELECT " +
                    "ID, ORDER_TIME, DRIVER_FK, CUSTOMER_FK, PICKUP_LOCATION, DESTINATION, PICKUP_TIME, " +
                    "ARRIVAL_TIME, DISTANCE_KM, PRICE, SERVICE_RATING " +
                    "FROM TAXI_SERVICE WHERE ID = ?"))
        {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {

                while(rs.next()) {
                    taxiService.setID(rs.getLong("ID"));
                    taxiService.setOrderTime(rs.getTimestamp("ORDER_TIME"));
                    TaxiDriver taxiDriverFK = new TaxiDriver(rs.getLong("DRIVER_FK"));
                    taxiService.setDriverFK(taxiDriverFK);
                    Customer customerFK = new Customer(rs.getLong("CUSTOMER_FK"));
                    taxiService.setCustomerFK(customerFK);
                    taxiService.setPickupLocation(rs.getString("PICKUP_LOCATION"));
                    taxiService.setDestination(rs.getString("DESTINATION"));
                    taxiService.setPickupTime(rs.getTimestamp("PICKUP_TIME"));
                    taxiService.setArrivalTime(rs.getTimestamp("ARRIVAL_TIME"));
                    taxiService.setDistanceKM(rs.getDouble("DISTANCE_KM"));
                    taxiService.setPrice(rs.getDouble("PRICE"));
                    taxiService.setServiceRating(rs.getDouble("SERVICE_RATING"));
                }

            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return taxiService;
    }

    @Override
    public TaxiService addTaxiService(TaxiService taxiService) {
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("INSERT INTO TAXI_SERVICE " +
                    "(ORDER_TIME, DRIVER_FK, CUSTOMER_FK, PICKUP_LOCATION, DESTINATION, PICKUP_TIME, " +
                    "ARRIVAL_TIME, DISTANCE_KM, PRICE, SERVICE_RATING) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {

                c.setAutoCommit(false);
                Timestamp currentTime = new Timestamp(System.currentTimeMillis());
                taxiService.setOrderTime(currentTime);

                ps.setTimestamp(1, taxiService.getOrderTime());
                ps.setLong(2, taxiService.getDriverFK().getID());
                ps.setLong(3, taxiService.getCustomerFK().getID());
                ps.setString(4, taxiService.getPickupLocation().trim());
                ps.setString(5, taxiService.getDestination().trim());
                ps.setTimestamp(6, taxiService.getPickupTime());
                ps.setTimestamp(7, taxiService.getArrivalTime());
                ps.setObject(8, taxiService.getDistanceKM(), java.sql.Types.DOUBLE);
                ps.setObject(9, taxiService.getPrice(), java.sql.Types.DOUBLE);
                ps.setObject(10, taxiService.getServiceRating(), java.sql.Types.DOUBLE);

                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

            try(PreparedStatement lastID = c.prepareStatement("SELECT ID FROM TAXI_SERVICE ORDER BY ID DESC LIMIT 1");
                ResultSet rsID = lastID.executeQuery()
            ) {
                while(rsID.next()) {
                    taxiService.setID(rsID.getLong("ID"));
                }
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return taxiService;
    }

    @Override
    public TaxiService updateTaxiService(long id, TaxiService taxiService) {
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("UPDATE TAXI_SERVICE " +
                    "SET DRIVER_FK = ?, CUSTOMER_FK = ?, PICKUP_LOCATION = ?, DESTINATION = ?, " +
                    "PICKUP_TIME = ?, ARRIVAL_TIME = ?, DISTANCE_KM = ?, PRICE = ?, SERVICE_RATING = ? " +
                    "WHERE ID = ?")) {

                c.setAutoCommit(false);

                ps.setLong(1, taxiService.getDriverFK().getID());
                ps.setLong(2, taxiService.getCustomerFK().getID());
                ps.setString(3, taxiService.getPickupLocation().trim());
                ps.setString(4, taxiService.getDestination().trim());
                ps.setTimestamp(5, taxiService.getPickupTime());
                ps.setTimestamp(6, taxiService.getArrivalTime());
                ps.setObject(7, taxiService.getDistanceKM(), java.sql.Types.DOUBLE);
                ps.setObject(8, taxiService.getPrice(), java.sql.Types.DOUBLE);
                ps.setObject(9, taxiService.getServiceRating(), java.sql.Types.DOUBLE);
                ps.setLong(10, id);

                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return getTaxiService(id);
    }

    @Override
    public TaxiService deleteTaxiService(long id) {
        TaxiService deletedTaxiService = getTaxiService(id);
        try(Connection c = DriverManager.getConnection(
                dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword())) {

            try(PreparedStatement ps = c.prepareStatement("DELETE FROM TAXI_SERVICE " +
                    "WHERE ID = ?")) {

                c.setAutoCommit(false);
                ps.setLong(1, id);
                ps.executeUpdate();
                c.commit();
                c.setAutoCommit(true);

            } catch(SQLException x) {
                c.rollback();
            }

        } catch(SQLException y) {
            LOGGER.info(Constant.SQL_EXCEPTION + y.getMessage());
        }
        return deletedTaxiService;
    }
}
