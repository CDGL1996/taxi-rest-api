package cl.taxi.api.dao;

import cl.taxi.api.models.Customer;

import java.util.List;

public interface CustomerDAO {

    List<Customer> getAllCustomers();
    Customer getCustomer(long id);
    Customer getCustomer(String email);
    Customer addCustomer(Customer customer);
    Customer updateCustomer(long id, Customer customer);
    Customer deleteCustomer(long id);

}
