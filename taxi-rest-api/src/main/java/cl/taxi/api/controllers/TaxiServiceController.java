package cl.taxi.api.controllers;

import cl.taxi.api.constants.Constant;
import cl.taxi.api.dto.TaxiServiceDTO;
import cl.taxi.api.services.TaxiDriverService;
import cl.taxi.api.services.TaxiServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/taxiservices")
public class TaxiServiceController {

    private final TaxiDriverService taxiDriverService;
    private final TaxiServiceService taxiServiceService;
    
    @Autowired
    public TaxiServiceController(TaxiDriverService taxiDriverService, TaxiServiceService taxiServiceService) {
    	this.taxiDriverService = taxiDriverService;
    	this.taxiServiceService = taxiServiceService;
    }

    @GetMapping(value = "/{firstname}/{lastname}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getTaxiServices(@PathVariable("firstname") String firstName,
                                                  @PathVariable("lastname") String lastName) {
    	
    	try {
    		taxiDriverService.getTaxiDriver(firstName, lastName);
    		List<TaxiServiceDTO> servicesList = taxiServiceService.getTaxiServices(firstName, lastName);
    		if(servicesList.isEmpty()) {
            	return new ResponseEntity<>("There's no taxi services for this taxi driver.", HttpStatus.NOT_FOUND);
            }
            else {
            	return new ResponseEntity<>(servicesList, HttpStatus.OK);
            }
    	} catch(ValidationException x) {
    		return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
    	}
 
    }

    @GetMapping(value = "/date/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getTaxiServicesByDate(@PathVariable("date") @DateTimeFormat(pattern = "dd-MM-yyyy") Date date) {
        List<TaxiServiceDTO> servicesList = taxiServiceService.getTaxiServices(date);
        if(servicesList.isEmpty()) {
            return new ResponseEntity<>("No taxi services have been found for this date", HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(servicesList, HttpStatus.OK);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getTaxiServicesByID(@PathVariable("id") long id) {
    	
    	try {
    		TaxiServiceDTO taxiService = taxiServiceService.getTaxiService(id);
    		if(taxiService.getID() == 0) {
                return new ResponseEntity<>("No taxi service has been found with this ID.", HttpStatus.NOT_FOUND);
            }
            else {
                return new ResponseEntity<>(taxiService, HttpStatus.OK);
            }
    	} catch(ValidationException x) {
    		return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
    	}
    	
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addTaxiService(@RequestBody TaxiServiceDTO taxiServiceDTO,
                                                HttpServletRequest request,
                                                HttpServletResponse response) {

        TaxiServiceDTO newTaxiService;

        try {
            newTaxiService = taxiServiceService.addTaxiService(taxiServiceDTO);
        } catch(ValidationException x) {
            return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
        }

        String presentURL = request.getRequestURL().toString();
        if(presentURL.endsWith("/")) {
            response.setHeader(Constant.LOCATION_HEADER, presentURL + newTaxiService.getID());
        } else {
            response.setHeader(Constant.LOCATION_HEADER, presentURL + "/" + newTaxiService.getID());
        }
        return new ResponseEntity<>(newTaxiService, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateTaxiService(@PathVariable("id") long id,
                                                   @RequestBody TaxiServiceDTO taxiServiceDTO,
                                                   HttpServletRequest request,
                                                   HttpServletResponse response) {

        TaxiServiceDTO existingService = taxiServiceService.getTaxiService(id);
        if(existingService.getID() == 0) {
            return new ResponseEntity<>("There's no taxi service using this ID.", HttpStatus.NOT_FOUND);
        } else {

            TaxiServiceDTO updatedTaxiService;

            try {
                updatedTaxiService = taxiServiceService.updateTaxiService(id, taxiServiceDTO);
            } catch(ValidationException x) {
                return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
            }

            response.setHeader(Constant.LOCATION_HEADER, request.getRequestURL().toString());
            return new ResponseEntity<>(updatedTaxiService, HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteTaxiService(@PathVariable("id") long id) {
        TaxiServiceDTO existingService = taxiServiceService.getTaxiService(id);
        if(existingService.getID() == 0) {
            return new ResponseEntity<>("There's no taxi service with this ID.", HttpStatus.NOT_FOUND);
        } else {
            TaxiServiceDTO deletedService = taxiServiceService.deleteTaxiService(id);
            return new ResponseEntity<>(deletedService, HttpStatus.OK);
        }
    }

}
