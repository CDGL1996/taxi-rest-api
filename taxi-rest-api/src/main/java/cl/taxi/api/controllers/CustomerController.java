package cl.taxi.api.controllers;

import cl.taxi.api.constants.Constant;
import cl.taxi.api.dto.CustomerDTO;

import cl.taxi.api.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerService customerService;
    
    @Autowired
    public CustomerController(CustomerService customerService) {
    	this.customerService = customerService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getCustomers() {
        List<CustomerDTO> customersList = customerService.getCustomers();
        if(customersList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else {
            return new ResponseEntity<>(customersList, HttpStatus.OK);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getCustomer(@PathVariable("id") long id) {
    	
    	try {
    		CustomerDTO customerFound = customerService.getCustomer(id);
    		if(customerFound.getID() == 0) {
    			return new ResponseEntity<>("There's no customer with this ID.", HttpStatus.NOT_FOUND);
    		} else {
                return new ResponseEntity<>(customerFound, HttpStatus.OK);
    		}
            
    	} catch(ValidationException x) {
    		return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
    	}
       
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addCustomer(@RequestBody CustomerDTO customer,
                                             HttpServletRequest request,
                                             HttpServletResponse response) {

        CustomerDTO existingCustomer = customerService.getCustomer(customer.getEmail());
        if (existingCustomer.getID() != 0) {
            return new ResponseEntity<>("There's already a customer with this email.", HttpStatus.CONFLICT);
        } else {
            CustomerDTO newCustomer;

            try {
                newCustomer = customerService.addCustomer(customer);
            } catch(ValidationException x) {
                return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
            }

            String presentURL = request.getRequestURL().toString();
            if(presentURL.endsWith("/")) {
                response.setHeader(Constant.LOCATION_HEADER, presentURL + newCustomer.getID());
            } else {
                response.setHeader(Constant.LOCATION_HEADER, presentURL + "/" + newCustomer.getID());
            }
            return new ResponseEntity<>(newCustomer, HttpStatus.CREATED);
        }

    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") long id,
                                                @RequestBody CustomerDTO customer,
                                                HttpServletRequest request,
                                                HttpServletResponse response) {

        CustomerDTO existingCustomer = customerService.getCustomer(id);
        if(existingCustomer.getID() == 0) {
            return new ResponseEntity<>("There's no customer with this ID.", HttpStatus.NOT_FOUND);
        } else {
            CustomerDTO existingName = customerService.getCustomer(customer.getEmail());
            if(existingName.getID() != 0 && existingName.getID() != id) {
                return new ResponseEntity<>("There's already another customer with this email.", HttpStatus.CONFLICT);
            } else {
                CustomerDTO updatedCustomer;

                try {
                    updatedCustomer = customerService.updateCustomer(id, customer);
                } catch(ValidationException x) {
                    return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
                }

                response.setHeader(Constant.LOCATION_HEADER, request.getRequestURL().toString());
                return new ResponseEntity<>(updatedCustomer, HttpStatus.OK);
            }
        }
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteCustomer(@PathVariable("id") long id) {
        CustomerDTO existingCustomer = customerService.getCustomer(id);
        if(existingCustomer.getID() == 0) {
            return new ResponseEntity<>("There's no customer with this ID.", HttpStatus.NOT_FOUND);
        } else {
            CustomerDTO deletedCustomer = customerService.deleteCustomer(id);
            return new ResponseEntity<>(deletedCustomer, HttpStatus.OK);
        }
    }

}
