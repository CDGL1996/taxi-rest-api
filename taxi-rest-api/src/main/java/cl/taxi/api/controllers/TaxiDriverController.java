package cl.taxi.api.controllers;

import cl.taxi.api.constants.Constant;
import cl.taxi.api.dto.TaxiDriverDTO;
import cl.taxi.api.services.TaxiDriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import java.util.List;

@RestController
@RequestMapping(value = "/taxidrivers")
public class TaxiDriverController {

    private final TaxiDriverService taxiDriverService;
    
    @Autowired
    public TaxiDriverController(TaxiDriverService taxiDriverService) {
    	this.taxiDriverService = taxiDriverService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getTaxiDrivers() {
        List<TaxiDriverDTO> taxiDriverList = taxiDriverService.getTaxiDrivers();
        if(taxiDriverList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else {
            return new ResponseEntity<>(taxiDriverList, HttpStatus.OK);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getTaxiDriver(@PathVariable("id") long id) {
    	try {
    		TaxiDriverDTO taxiDriverFound = taxiDriverService.getTaxiDriver(id);
    		if(taxiDriverFound.getID() == 0) {
                return new ResponseEntity<>("There's no taxi driver with this ID.", HttpStatus.NOT_FOUND);
    		} else {
                return new ResponseEntity<>(taxiDriverFound, HttpStatus.OK);
    		}
            
    	} catch(ValidationException x) {
    		return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
    	}
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addTaxiDriver(@RequestBody TaxiDriverDTO taxiDriverDTO,
                                              HttpServletRequest request,
                                              HttpServletResponse response) {

        TaxiDriverDTO existingTaxiDriver = taxiDriverService.getTaxiDriver(taxiDriverDTO.getName(), taxiDriverDTO.getLastname());
        if (existingTaxiDriver.getID() != 0) {
            return new ResponseEntity<>("There's already a taxi driver with this name and lastname.", HttpStatus.CONFLICT);
        } else {
            TaxiDriverDTO newTaxiDriver;

            try {
                newTaxiDriver = taxiDriverService.addTaxiDriver(taxiDriverDTO);
            } catch(ValidationException x) {
                return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
            }

            String presentURL = request.getRequestURL().toString();
            if(presentURL.endsWith("/")) {
                response.setHeader(Constant.LOCATION_HEADER, presentURL + newTaxiDriver.getID());
            } else {
                response.setHeader(Constant.LOCATION_HEADER, presentURL + "/" + newTaxiDriver.getID());
            }
            return new ResponseEntity<>(newTaxiDriver, HttpStatus.CREATED);
        }

    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateTaxiDriver(@PathVariable("id") long id,
                                                 @RequestBody TaxiDriverDTO taxiDriverDTO,
                                                 HttpServletRequest request,
                                                 HttpServletResponse response) {

        TaxiDriverDTO existingTaxiDriver = taxiDriverService.getTaxiDriver(id);
        if(existingTaxiDriver.getID() == 0) {
            return new ResponseEntity<>("There isn't a taxi driver with this ID.", HttpStatus.NOT_FOUND);
        } else {
            TaxiDriverDTO existingName = taxiDriverService.getTaxiDriver(taxiDriverDTO.getName(), taxiDriverDTO.getLastname());
            if(existingName.getID() != 0 && existingName.getID() != id) {
                return new ResponseEntity<>("There's already another taxi driver with this name and lastname.", HttpStatus.CONFLICT);
            } else {
                TaxiDriverDTO updatedTaxiDriver;

                try {
                    updatedTaxiDriver = taxiDriverService.updateTaxiDriver(id, taxiDriverDTO);
                } catch(ValidationException x) {
                    return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
                }

                response.setHeader(Constant.LOCATION_HEADER, request.getRequestURL().toString());
                return new ResponseEntity<>(updatedTaxiDriver, HttpStatus.OK);
            }
        }
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteTaxiDriver(@PathVariable("id") long id) {
        TaxiDriverDTO existingTaxiDriver = taxiDriverService.getTaxiDriver(id);
        if(existingTaxiDriver.getID() == 0) {
            return new ResponseEntity<>("There's no taxi driver with this ID.", HttpStatus.NOT_FOUND);
        } else {
            TaxiDriverDTO deletedTaxiDriver = taxiDriverService.deleteTaxiDriver(id);
            return new ResponseEntity<>(deletedTaxiDriver, HttpStatus.OK);
        }
    }

}
