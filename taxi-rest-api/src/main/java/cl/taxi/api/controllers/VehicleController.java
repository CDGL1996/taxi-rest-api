package cl.taxi.api.controllers;

import cl.taxi.api.constants.Constant;
import cl.taxi.api.dto.VehicleDTO;
import cl.taxi.api.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;
    
    @Autowired
    public VehicleController(VehicleService vehicleService) {
    	this.vehicleService = vehicleService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getVehicles() {
        List<VehicleDTO> vehiclesList = vehicleService.getVehicles();
        if(vehiclesList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else {
            return new ResponseEntity<>(vehiclesList, HttpStatus.OK);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getVehicle(@PathVariable("id") long id) {
    	
    	try {
    		VehicleDTO vehicleFound = vehicleService.getVehicle(id);
    		if(vehicleFound.getID() == 0) {
                return new ResponseEntity<>("There's no vehicle with this ID.", HttpStatus.NOT_FOUND);
    		} else {
                return new ResponseEntity<>(vehicleFound, HttpStatus.OK);
    		}
            
    	} catch(ValidationException x) {
    		return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
    	}
    	
    }

    @GetMapping(value = "/plate/{plate}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getVehicleByPlate(@PathVariable("plate") String plateNumber) {
        String vehiclePlate = plateNumber.trim().toUpperCase();
        try {
    		VehicleDTO vehicleFound = vehicleService.getVehicle(vehiclePlate);
            return new ResponseEntity<>(vehicleFound, HttpStatus.OK);
            
    	} catch(ValidationException x) {
    		return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
    	}
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addVehicle(@RequestBody VehicleDTO vehicleDTO,
                                             HttpServletRequest request,
                                             HttpServletResponse response) {

        VehicleDTO existingVehicle = vehicleService.getVehicle(vehicleDTO.getLicensePlate());
        if (existingVehicle.getLicensePlate() != null) {
            return new ResponseEntity<>("There's already a vehicle with this license plate.", HttpStatus.CONFLICT);
        } else {
            VehicleDTO newVehicle;

            try {
                newVehicle = vehicleService.addVehicle(vehicleDTO);
            } catch (ValidationException x) {
                return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
            }

            String presentURL = request.getRequestURL().toString();

            if(presentURL.endsWith("/")) {
                response.setHeader(Constant.LOCATION_HEADER, presentURL + newVehicle.getID());
            } else {
                response.setHeader(Constant.LOCATION_HEADER, presentURL + "/" + newVehicle.getID());
            }
            return new ResponseEntity<>(newVehicle, HttpStatus.CREATED);
        }

    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateVehicle(@PathVariable("id") long id,
                                                @RequestBody VehicleDTO vehicleDTO,
                                                HttpServletRequest request,
                                                HttpServletResponse response) {

        VehicleDTO existingVehicle = vehicleService.getVehicle(id);
        if(existingVehicle.getID() == 0) {
            return new ResponseEntity<>("There's no vehicle with this ID.", HttpStatus.NOT_FOUND);
        } else {
            VehicleDTO existingPlate = vehicleService.getVehicle(vehicleDTO.getLicensePlate());
            if(existingPlate.getID() != 0 && existingPlate.getID() != id) {
                return new ResponseEntity<>("There's already another vehicle with this license plate.", HttpStatus.CONFLICT);
            } else {
                VehicleDTO updatedVehicle;
                try {
                    updatedVehicle = vehicleService.updateVehicle(id, vehicleDTO);
                } catch (ValidationException x) {
                    return new ResponseEntity<>(x.getMessage(), HttpStatus.BAD_REQUEST);
                }

                response.setHeader(Constant.LOCATION_HEADER, request.getRequestURL().toString());
                return new ResponseEntity<>(updatedVehicle, HttpStatus.OK);
            }
        }
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteVehicle(@PathVariable("id") long id) {
        VehicleDTO existingVehicle = vehicleService.getVehicle(id);
        if(existingVehicle.getID() == 0) {
            return new ResponseEntity<>("There's no vehicle with this ID.", HttpStatus.NOT_FOUND);
        } else {
            VehicleDTO deletedVehicle = vehicleService.deleteVehicle(id);
            return new ResponseEntity<>(deletedVehicle, HttpStatus.OK);
        }
    }

}
