package cl.taxi.api.dto;

import cl.taxi.api.models.Customer;
import java.util.Date;

public class CustomerDTO {

    private long id;
    private String name;
    private String lastname;
    private double rating;
    private Date joinDate;
    private String phone;
    private String email;

    // Constructors

    public CustomerDTO() { }

    public CustomerDTO(Customer customer) {
        this.id = customer.getID();
        this.name = customer.getFirstName();
        this.lastname = customer.getLastName();
        this.rating = customer.getUserRating();
        this.joinDate = customer.getJoinDate();
        this.phone = customer.getPhoneNumber();
        this.email = customer.getEmail();
    }

    // Getters

    public long getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public Double getRating() {
        return rating;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    // Setters

    public void setID(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
