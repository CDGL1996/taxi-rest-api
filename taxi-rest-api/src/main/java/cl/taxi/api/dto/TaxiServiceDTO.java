package cl.taxi.api.dto;

import cl.taxi.api.models.Customer;
import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.TaxiService;

import java.sql.Timestamp;

public class TaxiServiceDTO {

    private long id;
    private Timestamp orderTime;
    private TaxiDriver driver;
    private Customer customer;
    private String pickupLocation;
    private String destination;
    private Timestamp pickup;
    private Timestamp arrival;
    private Double kilometers = null;
    private Double price = null;
    private Double serviceRating = null;

    // Constructors

    public TaxiServiceDTO() { }

    public TaxiServiceDTO(TaxiService taxiService) {
        this.id = taxiService.getID();
        this.orderTime = taxiService.getOrderTime();
        this.driver = taxiService.getDriverFK();
        this.customer = taxiService.getCustomerFK();
        this.pickupLocation = taxiService.getPickupLocation();
        this.destination = taxiService.getDestination();
        this.pickup = taxiService.getPickupTime();
        this.arrival = taxiService.getArrivalTime();
        this.kilometers = taxiService.getDistanceKM();
        this.price = taxiService.getPrice();
        this.serviceRating = taxiService.getServiceRating();
    }

    // Getters

    public long getID() {
        return id;
    }

    public Timestamp getOrderTime() {
        return orderTime;
    }

    public TaxiDriver getDriver() {
        return driver;
    }

    public Customer getCustomer() {
        return customer;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public String getDestination() {
        return destination;
    }

    public Timestamp getPickup() {
        return pickup;
    }

    public Timestamp getArrival() {
        return arrival;
    }

    public Double getKilometers() {
        return kilometers;
    }

    public Double getPrice() {
        return price;
    }

    public Double getServiceRating() {
        return serviceRating;
    }

    // Setters

    public void setID(long id) {
        this.id = id;
    }

    public void setOrderTime(Timestamp orderTime) {
        this.orderTime = orderTime;
    }

    public void setDriver(TaxiDriver driver) {
        this.driver = driver;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setPickup(Timestamp pickup) {
        this.pickup = pickup;
    }

    public void setArrival(Timestamp arrival) {
        this.arrival = arrival;
    }

    public void setKilometers(Double kilometers) {
        this.kilometers = kilometers;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setServiceRating(Double serviceRating) {
        this.serviceRating = serviceRating;
    }
}
