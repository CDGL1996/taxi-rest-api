package cl.taxi.api.dto;

import cl.taxi.api.models.Vehicle;

import java.util.Date;

public class VehicleDTO {

    private long id;
    private String brand;
    private String model;
    private String color;
    private int seats;
    private Date registrationDate;
    private String licensePlate;
    private Boolean active;

    // Constructors

    public VehicleDTO() { }

    public VehicleDTO(Vehicle vehicle) {
        this.id = vehicle.getID();
        this.brand = vehicle.getVehicleBrand();
        this.model = vehicle.getVehicleModel();
        this.color = vehicle.getVehicleColor();
        this.seats = vehicle.getVehicleSeats();
        this.registrationDate = vehicle.getRegistrationDate();
        this.licensePlate = vehicle.getLicensePlate();
        this.active = vehicle.isActive();
    }

    // Getters

    public long getID() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getSeats() {
        return seats;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public Boolean isActive() {
        return active;
    }

    // Setters

    public void setID(long id) {
        this.id = id;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
