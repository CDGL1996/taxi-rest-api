package cl.taxi.api.dto;

import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.Vehicle;

import java.util.Date;

public class TaxiDriverDTO {

    private long id;
    private String name;
    private String lastname;
    private double rating;
    private Date joinDate;
    private String phone;
    private int jobsDone;
    private Vehicle vehicle;

    // Constructor

    public TaxiDriverDTO() { }

    public TaxiDriverDTO(TaxiDriver taxiDriver) {
        this.id = taxiDriver.getID();
        this.name = taxiDriver.getFirstName();
        this.lastname = taxiDriver.getLastName();
        this.rating = taxiDriver.getDriverRating();
        this.joinDate = taxiDriver.getJoinDate();
        this.phone = taxiDriver.getPhoneNumber();
        this.jobsDone = taxiDriver.getJobsPerformed();
        this.vehicle = taxiDriver.getVehicleFK();
    }

    // Getters

    public long getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public double getRating() {
        return rating;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public String getPhone() {
        return phone;
    }

    public int getJobsDone() {
        return jobsDone;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    // Setters

    public void setID(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setJobsDone(int jobsDone) {
        this.jobsDone = jobsDone;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
}
