package cl.taxi.api.services;

import cl.taxi.api.dao.implementations.TaxiDriverDAOImpl;
import cl.taxi.api.dao.implementations.VehicleDAOImpl;
import cl.taxi.api.dto.TaxiDriverDTO;
import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TaxiDriverService {

    private final TaxiDriverDAOImpl taxiDriverDAOImpl;
    private final VehicleDAOImpl vehicleDAOImpl;
    
    @Autowired
    public TaxiDriverService(TaxiDriverDAOImpl taxiDriverDAOImpl, VehicleDAOImpl vehicleDAOImpl) {
    	this.taxiDriverDAOImpl = taxiDriverDAOImpl;
    	this.vehicleDAOImpl = vehicleDAOImpl;
    }

    public List<TaxiDriverDTO> getTaxiDrivers() {
        List<TaxiDriverDTO> taxiDriverListDTO = new ArrayList<>();
        List<TaxiDriver> taxiDriverList = taxiDriverDAOImpl.getAllTaxiDrivers();

        for(int i = 0; i < taxiDriverList.size(); i++) {
            TaxiDriverDTO taxiDriverDTO = new TaxiDriverDTO(taxiDriverList.get(i));
            taxiDriverListDTO.add(taxiDriverDTO);
        }

        return taxiDriverListDTO;
    }

    public TaxiDriverDTO getTaxiDriver(long id) {
    	TaxiDriver taxiDriver = new TaxiDriver();
    	taxiDriver.validateID(id);
    	taxiDriver = taxiDriverDAOImpl.getTaxiDriver(id);
    	
        return new TaxiDriverDTO(taxiDriver);
    }

    public TaxiDriverDTO getTaxiDriver(String firstName, String lastName) {
    	TaxiDriver taxiDriver = new TaxiDriver();
    	taxiDriver.validateFirstName(firstName);
    	taxiDriver.validateLastName(lastName);
    	taxiDriver = taxiDriverDAOImpl.getTaxiDriver(firstName, lastName);
    	
        return new TaxiDriverDTO(taxiDriver);
    }

    public TaxiDriverDTO addTaxiDriver(TaxiDriverDTO dto) {
        TaxiDriver taxiDriver = new TaxiDriver(dto);

        taxiDriver.validateFirstName(taxiDriver.getFirstName());
        taxiDriver.validateLastName(taxiDriver.getLastName());
        taxiDriver.validateRating(taxiDriver.getDriverRating());
        taxiDriver.validatePhoneNumber(taxiDriver.getPhoneNumber());
        taxiDriver.validateJobs(taxiDriver.getJobsPerformed());
        taxiDriver.validateVehicle(taxiDriver.getVehicleFK());
        Vehicle vehicleFound = vehicleDAOImpl.getVehicle(taxiDriver.getVehicleFK().getID());
        if(vehicleFound.getID() == 0) { throw new ValidationException("There's no vehicle with this ID."); }

        return new TaxiDriverDTO(taxiDriverDAOImpl.addTaxiDriver(taxiDriver));
    }

    public TaxiDriverDTO updateTaxiDriver(long id, TaxiDriverDTO dto) {
        TaxiDriver taxiDriver = new TaxiDriver(dto);

        taxiDriver.validateFirstName(taxiDriver.getFirstName());
        taxiDriver.validateLastName(taxiDriver.getLastName());
        taxiDriver.validateRating(taxiDriver.getDriverRating());
        taxiDriver.validatePhoneNumber(taxiDriver.getPhoneNumber());
        taxiDriver.validateJobs(taxiDriver.getJobsPerformed());
        taxiDriver.validateVehicle(taxiDriver.getVehicleFK());
        Vehicle vehicleFound = vehicleDAOImpl.getVehicle(taxiDriver.getVehicleFK().getID());
        if(vehicleFound.getID() == 0) { throw new ValidationException("There's no vehicle with this ID."); }

        return new TaxiDriverDTO(taxiDriverDAOImpl.updateTaxiDriver(id, taxiDriver));
    }

    public TaxiDriverDTO deleteTaxiDriver(long id) {
        return new TaxiDriverDTO(taxiDriverDAOImpl.deleteTaxiDriver(id));
    }

}
