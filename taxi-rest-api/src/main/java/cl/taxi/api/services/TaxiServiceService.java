package cl.taxi.api.services;

import cl.taxi.api.dao.implementations.CustomerDAOImpl;
import cl.taxi.api.dao.implementations.TaxiDriverDAOImpl;
import cl.taxi.api.dao.implementations.TaxiServiceDAOImpl;
import cl.taxi.api.dto.TaxiServiceDTO;
import cl.taxi.api.models.Customer;
import cl.taxi.api.models.TaxiDriver;
import cl.taxi.api.models.TaxiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TaxiServiceService {

    private final TaxiServiceDAOImpl taxiServiceDAOImpl;
    private final TaxiDriverDAOImpl taxiDriverDAOImpl;
    private final CustomerDAOImpl customerDAOImpl;
    
    @Autowired
    public TaxiServiceService(TaxiServiceDAOImpl taxiServiceDAOImpl, TaxiDriverDAOImpl taxiDriverDAOImpl, CustomerDAOImpl customerDAOImpl) {
    	this.taxiServiceDAOImpl = taxiServiceDAOImpl;
    	this.taxiDriverDAOImpl = taxiDriverDAOImpl;
    	this.customerDAOImpl = customerDAOImpl;
    }

    public List<TaxiServiceDTO> getTaxiServices(String driverName, String driverLastName) {
        List<TaxiServiceDTO> serviceListDTO = new ArrayList<>();
        
        TaxiDriver taxiDriver = new TaxiDriver();
        taxiDriver.validateFirstName(driverName);
        taxiDriver.validateLastName(driverLastName);
        
        List<TaxiService> serviceList = taxiServiceDAOImpl.getTaxiServices(driverName, driverLastName);

        for(int i = 0; i < serviceList.size(); i++) {
            TaxiServiceDTO serviceDTO = new TaxiServiceDTO(serviceList.get(i));
            serviceListDTO.add(serviceDTO);
        }

        return serviceListDTO;
    }

    public List<TaxiServiceDTO> getTaxiServices(Date date) {
        List<TaxiServiceDTO> serviceListDTO = new ArrayList<>();
        List<TaxiService> serviceList = taxiServiceDAOImpl.getTaxiServices(date);

        for(int i = 0; i < serviceList.size(); i++) {
            TaxiServiceDTO serviceDTO = new TaxiServiceDTO(serviceList.get(i));
            serviceListDTO.add(serviceDTO);
        }

        return serviceListDTO;
    }

    public TaxiServiceDTO getTaxiService(long id) {
    	TaxiService taxiService = new TaxiService();
    	taxiService.validateID(id);
    	
    	taxiService = taxiServiceDAOImpl.getTaxiService(id);
        return new TaxiServiceDTO(taxiService);
    }

    public TaxiServiceDTO addTaxiService(TaxiServiceDTO dto) {
        TaxiService taxiService = new TaxiService(dto);

        taxiService.validateDriverFK(taxiService.getDriverFK());
        TaxiDriver driverFound = taxiDriverDAOImpl.getTaxiDriver(taxiService.getDriverFK().getID());
        if(driverFound.getID() == 0) { throw new ValidationException("There's no taxi driver with this ID."); }
        taxiService.validateCustomerFK(taxiService.getCustomerFK());
        Customer customerFound = customerDAOImpl.getCustomer(taxiService.getCustomerFK().getID());
        if(customerFound.getID() == 0) { throw new ValidationException("There's no customer with this ID."); }
        taxiService.validatePickupLocation(taxiService.getPickupLocation());
        taxiService.validateDestination(taxiService.getDestination());
        taxiService.validatePickupTime(taxiService.getPickupTime());
        taxiService.validateArrivalTime(taxiService.getArrivalTime());
        taxiService.validateDistance(taxiService.getDistanceKM());
        taxiService.validatePrice(taxiService.getPrice());
        taxiService.validateRating(taxiService.getServiceRating());

        return new TaxiServiceDTO(taxiServiceDAOImpl.addTaxiService(taxiService));
    }

    public TaxiServiceDTO updateTaxiService(long id, TaxiServiceDTO dto) {
        TaxiService taxiService = new TaxiService(dto);

        taxiService.validateDriverFK(taxiService.getDriverFK());
        TaxiDriver driverFound = taxiDriverDAOImpl.getTaxiDriver(taxiService.getDriverFK().getID());
        if(driverFound.getID() == 0) { throw new ValidationException("There's no taxi driver with this ID."); }
        taxiService.validateCustomerFK(taxiService.getCustomerFK());
        Customer customerFound = customerDAOImpl.getCustomer(taxiService.getCustomerFK().getID());
        if(customerFound.getID() == 0) { throw new ValidationException("There's no customer with this ID."); }
        taxiService.validatePickupLocation(taxiService.getPickupLocation());
        taxiService.validateDestination(taxiService.getDestination());
        taxiService.validatePickupTime(taxiService.getPickupTime());
        taxiService.validateArrivalTime(taxiService.getArrivalTime());
        taxiService.validateDistance(taxiService.getDistanceKM());
        taxiService.validatePrice(taxiService.getPrice());
        taxiService.validateRating(taxiService.getServiceRating());

        dto = new TaxiServiceDTO(taxiServiceDAOImpl.updateTaxiService(id, taxiService));
        return dto;
    }

    public TaxiServiceDTO deleteTaxiService(long id) {
        return new TaxiServiceDTO(taxiServiceDAOImpl.deleteTaxiService(id));
    }


}
