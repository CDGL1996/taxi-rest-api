package cl.taxi.api.services;

import cl.taxi.api.dao.implementations.VehicleDAOImpl;
import cl.taxi.api.dto.VehicleDTO;
import cl.taxi.api.models.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VehicleService {

    private final VehicleDAOImpl vehicleDAOImpl;
    
    @Autowired
    public VehicleService(VehicleDAOImpl vehicleDAOImpl) {
    	this.vehicleDAOImpl = vehicleDAOImpl;
    }

    public List<VehicleDTO> getVehicles() {
        List<VehicleDTO> vehicleListDTO = new ArrayList<>();
        List<Vehicle> vehicleList = vehicleDAOImpl.getAllVehicles();

        for(int i = 0; i < vehicleList.size(); i++) {
            VehicleDTO vehicleDTO = new VehicleDTO(vehicleList.get(i));
            vehicleListDTO.add(vehicleDTO);
        }
        return vehicleListDTO;
    }

    public VehicleDTO getVehicle(long id) {
    	Vehicle vehicle = new Vehicle();
    	vehicle.validateID(id);
    	
    	vehicle = vehicleDAOImpl.getVehicle(id);
        return new VehicleDTO(vehicle);
    }

    public VehicleDTO getVehicle(String plateNumber) {
        Vehicle vehicle = new Vehicle();
        vehicle.validateLicensePlate(plateNumber);
        String plate = plateNumber.toUpperCase().trim();
        vehicle = vehicleDAOImpl.getVehicle(plate);
        return new VehicleDTO(vehicle);
    }

    public VehicleDTO addVehicle(VehicleDTO dto) {
        Vehicle vehicle = new Vehicle(dto);

        vehicle.validateBrand(vehicle.getVehicleBrand());
        vehicle.validateModel(vehicle.getVehicleModel());
        vehicle.validateColor(vehicle.getVehicleColor());
        vehicle.validateSeats(vehicle.getVehicleSeats());
        vehicle.validateLicensePlate(vehicle.getLicensePlate());
        vehicle.validateActive(vehicle.isActive());

        return new VehicleDTO(vehicleDAOImpl.addVehicle(vehicle));
    }

    public VehicleDTO updateVehicle(long id, VehicleDTO dto) {
        Vehicle vehicle = new Vehicle(dto);
        
        vehicle.validateID(id);

        vehicle.validateBrand(vehicle.getVehicleBrand());
        vehicle.validateModel(vehicle.getVehicleModel());
        vehicle.validateColor(vehicle.getVehicleColor());
        vehicle.validateSeats(vehicle.getVehicleSeats());
        vehicle.validateLicensePlate(vehicle.getLicensePlate());
        vehicle.validateActive(vehicle.isActive());

        return new VehicleDTO(vehicleDAOImpl.updateVehicle(id, vehicle));
    }

    public VehicleDTO deleteVehicle(long id) {
    	Vehicle vehicle = new Vehicle();
    	vehicle.validateID(id);
    	vehicle = vehicleDAOImpl.deleteVehicle(id);
        return new VehicleDTO(vehicle);
    }

}
