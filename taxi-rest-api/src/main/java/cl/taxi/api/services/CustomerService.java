package cl.taxi.api.services;

import cl.taxi.api.dao.implementations.CustomerDAOImpl;
import cl.taxi.api.dto.CustomerDTO;
import cl.taxi.api.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {

    private final CustomerDAOImpl customerDAOImpl;
    
    @Autowired
    public CustomerService(CustomerDAOImpl customerDAOImpl) {
    	this.customerDAOImpl = customerDAOImpl;
    }

    public List<CustomerDTO> getCustomers() {
        List<Customer> customersList = customerDAOImpl.getAllCustomers();
        List<CustomerDTO> customerListDTO = new ArrayList<>();
        
        for(int i = 0; i < customersList.size(); i++) {
            CustomerDTO dto = new CustomerDTO(customersList.get(i));
            customerListDTO.add(dto);
        }

        return customerListDTO;
    }

    public CustomerDTO getCustomer(long id) {
    	
    	Customer foundCustomer = new Customer();
    	foundCustomer.validateID(id);
    	foundCustomer = customerDAOImpl.getCustomer(id);
    	
        return new CustomerDTO(foundCustomer);
    }

    public CustomerDTO getCustomer(String email) {
        Customer foundCustomer = new Customer();
    	foundCustomer.validateEmail(email);
    	String userEmail = email.toUpperCase().trim();
    	foundCustomer = customerDAOImpl.getCustomer(userEmail);
    	
        return new CustomerDTO(foundCustomer);
    }

    public CustomerDTO addCustomer(CustomerDTO dto) {
        Customer customer = new Customer(dto);

        customer.validateFirstName(customer.getFirstName());
        customer.validateLastName(customer.getLastName());
        customer.validateRating(customer.getUserRating());
        customer.validatePhoneNumber(customer.getPhoneNumber());
        customer.validateEmail(customer.getEmail());

        return new CustomerDTO(customerDAOImpl.addCustomer(customer));
    }

    public CustomerDTO updateCustomer(long id, CustomerDTO dto) {
        Customer customer = new Customer(dto);

        customer.validateFirstName(customer.getFirstName());
        customer.validateLastName(customer.getLastName());
        customer.validateRating(customer.getUserRating());
        customer.validatePhoneNumber(customer.getPhoneNumber());
        customer.validateEmail(customer.getEmail());

        return new CustomerDTO(customerDAOImpl.updateCustomer(id, customer));
    }

    public CustomerDTO deleteCustomer(long id) {
    	Customer deletedCustomer = new Customer();
    	deletedCustomer.validateID(id);
    	deletedCustomer = customerDAOImpl.deleteCustomer(id);
        return new CustomerDTO(deletedCustomer);
    }

}
